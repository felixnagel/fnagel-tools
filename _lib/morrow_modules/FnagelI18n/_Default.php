<?php

namespace app\modules\FnagelI18n;
use Morrow\Factory;
use Morrow\Debug;

class _Default extends Factory {
	protected $_i18n_cookie_name;
	protected $_cookie_expire_time;
	protected $_possible_languages;
	protected $_lang = '';

	public function __construct(){
		$this->_i18n_cookie_name = $this->Config->get('modules.FnagelI18n.cookie-name');
		$this->_cookie_expire_time = $this->Config->get('modules.FnagelI18n.cookie-expire-time');
		$this->_possible_languages = $this->Config->get('languages.possible');
		$this->_initLang();
	}

	protected function _initLang(){
		$lang = '';
		if($this->Session->get($this->_i18n_cookie_name, false)){
			$lang = $this->Session->get($this->_i18n_cookie_name);
		}else{
			if(isset($_COOKIE[$this->_i18n_cookie_name])){
				$lang = $_COOKIE[$this->_i18n_cookie_name];
			}
		}

		if(!in_array($lang, $this->_possible_languages) || !strlen($lang)){
			$lang = $this->_possible_languages[0];
		}

		Factory::load('Language')->set($lang);

		$this->_lang = $lang;
	}
}
