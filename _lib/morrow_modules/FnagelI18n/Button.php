<?php

namespace app\modules\FnagelI18n;
use Morrow\Factory;
use Morrow\Debug;

class Button extends _Default {
	public function run($dom){
		$this->Views_Serpent->setContent('current_lang', $this->_lang);

		$next_key = array_search($this->_lang, $this->_possible_languages) + 1;
		if(!isset($this->_possible_languages[$next_key])){
			$next_key = 0;
		}
		$next_lang = $this->_possible_languages[$next_key];
		$this->Views_Serpent->setContent('next_lang', $next_lang);

		$dom->append('head', '<link rel="stylesheet" href="modules/FnagelI18n/public/css/style.css" />');

		return $this->Views_Serpent;
	}

}

