<?php

namespace app\modules\FnagelI18n;
use Morrow\Factory;
use Morrow\Debug;

class _Pre extends _Default {
	public function run(){
		// for debug: delete cookie
		// setcookie($this->_i18n_cookie_name, null, -1);

		if($this->Input->get('FnagelI18n-switch-lang')){
			$this->_switchLang($this->Input->get('FnagelI18n-switch-lang'));
		}
	}

	protected function _switchLang($lang){
		$this->Session->set($this->_i18n_cookie_name, $lang);
		setcookie($this->_i18n_cookie_name, $lang, time() + $this->_cookie_expire_time);
		$this->_initLang();
	}
}
