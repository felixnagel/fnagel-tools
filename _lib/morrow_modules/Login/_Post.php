<?php

namespace app\modules\Login;
use Morrow\Factory;
use Morrow\Debug;

class _Post extends _Default{
	public function run(){
		if(!$this->_get_user() && $this->_login()){
			$this->Event->trigger('Login|login-successful');
		}
	}
}
