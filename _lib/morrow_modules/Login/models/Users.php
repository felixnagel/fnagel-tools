<?php

namespace app\modules\Login\models;

use Morrow\Factory;
use Morrow\Debug;
use app\modules\Login\lib\Crypt;

class Users extends AlphaModel{
	protected $_table = 'users';
	protected $_allowed_insert_fields = [
		'is_admin',
		'login',
		'pass',
	];
	protected $_allowed_update_fields = [
		'is_admin',
		'last_login_at',
		'login',
		'pass',
	];

	public function update($data, $conditions){
		if(isset($data['pass'])){
			$data['pass'] = Crypt::createHash($data['pass']);
		}
		return parent::update($data, $conditions);
	}


}
