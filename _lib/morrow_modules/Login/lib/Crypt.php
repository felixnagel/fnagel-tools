<?php

namespace app\modules\Login\lib;
use Morrow\Debug;
use Morrow\Factory;

class Crypt{

	private static $_characterStock          = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%*_-';
	private static $_hashCost                = 5;
	private static $_passwordMinLength       = 8;

	/**
	 * Create a hash from any string
	 *
	 * @param	string	$string		The string to create the hash from
	 * @return 	string				The generated hash
	 */
	public static function createHash($string){
		return password_hash($string, PASSWORD_BCRYPT, array('cost' => self::$_hashCost));
	}

	/**
	 * Create a random password
	 *
	 * @return 	string	The genertaed password
	 */
	public static function createPassword(){
		return self::generateRandomString(self::$_passwordMinLength);
	}

	/**
	 * Compare a password with its hash
	 *
	 * @param	string	$password	The password
	 * @param  	string	$hash		The hash
	 * @return 	bool				Password is valid
	 */
	public static function verifyPassword($password, $hash){
		return password_verify($password, $hash);
	}

	/**
	 * Create a random string
	 *
	 * @param  	int		$length		The length of the string
	 * @return 	string				The generated string
	 */
	public static function generateRandomString($length){
		$randomString = '';
		$i = 0;

		while($i++ < $length){
			$randomString .= substr(self::$_characterStock, mt_rand(0, (strlen(self::$_characterStock) - 1)), 1);
		}

		return $randomString;
	}
}
