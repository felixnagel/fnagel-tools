<?php

namespace app\modules\Login;
use Morrow\Factory;
use Morrow\Debug;

class _Pre extends _Default {
	public function run() {
		$this->Event->on('Login|get_current_user', function($event){
			return $this->_get_user();
		});

		$this->Event->on('Login|logout', function($event){
			$this->_logout();
		});
	}
}
