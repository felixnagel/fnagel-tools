<?php

namespace app\modules\Login;
use Morrow\Factory;
use Morrow\Debug;

class _Default extends Factory {
	public function __construct(){
		return $this->Views_Serpent;
	}

	protected function _get_user($userLogin = null){
		$user = [];

		if($userLogin){
			$user = Factory::load('\\app\\modules\\Login\\models\\Users')->getSingle(['login' => $userLogin]);
		}else{
			$userId = $this->Session->get('Login|user_id', false);
			if($userId){
				$user = Factory::load('\\app\\modules\\Login\\models\\Users')->getSingle($userId);
			}
		}

		if($user){
			return $user;
		}
		return false;
	}

	protected function _login(){
		if(!$this->Input->get('login') || !$this->Input->get('pass')){
			return false;
		}

		$user = $this->_get_user($this->Input->get('login'));
		if(!$user){
			return false;
		}

		$authentification = lib\Crypt::verifyPassword($this->Input->get('pass'), $user['pass']);

		if(!$authentification){
			return false;
		}

		Factory::load('\\app\\modules\\Login\\models\\Users')->update(['last_login_at' => Factory::load('\datetime')->format('Y-m-d H:i:s')], $user['id']);

		$this->Session->set('Login|user_id', $user['id']);
		return true;
	}

	protected function _logout(){
		$this->Session->delete('Login|user_id');
	}




}
