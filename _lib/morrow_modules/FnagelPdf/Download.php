<?php

namespace app\modules\FnagelPdf;
use Morrow\Factory;
use Morrow\Debug;

class Download extends Factory {
	private $_html_folder;
	private $_pdf_folder;

	public function __construct(){
		$this->_html_folder = $this->Config->get('modules.FnagelPdf.html_folder');
		$this->_pdf_folder = $this->Config->get('modules.FnagelPdf.pdf_folder');
	}

	public function run($dom){
		if($this->Input->get('pdf', false)){
			$content = $dom->get();

			$pdf_path = $this->_createPdfFile($content);
			$this->_download($pdf_path);
		}
	}

	private function _download($pdf_path){
		$pdfContent = file_get_contents($pdf_path);
		unlink($pdf_path);

		if($this->Input->get('filename', false)){
			$filename = $this->Input->get('filename') . '.pdf';
		}else{
			$filename = 'pdf_download_' . date('Y-m-d') . '.pdf';
		}

		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename=' . $filename);
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');

		echo $pdfContent;

		die();
	}

	private function _createPdfFile($content){
		$html_path = $this->_getFilePath($content, 'html');
		$pdf_path = $this->_getFilePath($content, 'pdf');
		file_put_contents($html_path, $content);

		$wkhtmltopdf_path = $this->Config->get('modules.FnagelPdf.wkhtmltopdf_path');
		$wkhtmltopdf_options = $this->Config->get('modules.FnagelPdf.wkhtmltopdf_options');

		if (in_array($_SERVER['HTTP_HOST'], ['staging.ministry.de', '172.20.0.206'])) {
			$command = sprintf('xvfb-run --server-args="-screen 0, 1024x768x24" %s %s %s %s 2>&1', $wkhtmltopdf_path, $wkhtmltopdf_options, $html_path, $pdf_path);
			exec($command, $return, $code);
		} else {
			$command = sprintf('%s %s %s %s 2>&1', $wkhtmltopdf_path, $wkhtmltopdf_options, $html_path, $pdf_path);
			exec($command, $return, $code);
		}

		echo implode('', $return);
		die($command);

		if($code !== 0){
			die(implode('', $return));
		}

		unlink($html_path);

		return $pdf_path;
	}

	private function _getFilePath($content, $type){
		$filename = md5($content) . '.' . $type;
		if($type === 'html'){
			if(!is_dir($this->_html_folder)){
				mkdir($this->_html_folder, true, 0777);
			}
			return $this->_html_folder . $filename;
		}
		if($type === 'pdf'){
			if(!is_dir($this->_pdf_folder)){
				mkdir($this->_pdf_folder, true, 0777);
			}
			return $this->_pdf_folder . $filename;
		}
	}
}
