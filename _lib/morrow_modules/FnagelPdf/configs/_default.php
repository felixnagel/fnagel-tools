<?php

return [
	'html_folder'         => STORAGE_PUBLIC_PATH . 'fnagelPdf/html/',
	'pdf_folder'          => STORAGE_PUBLIC_PATH . 'fnagelPdf/pdf/',
	'wkhtmltopdf_path'    => '/usr/local/bin/wkhtmltopdf',
	'wkhtmltopdf_options' => '--margin-top 0 --margin-right 0 --margin-bottom 0 --margin-left 0 --no-outline --page-size A4',
];
