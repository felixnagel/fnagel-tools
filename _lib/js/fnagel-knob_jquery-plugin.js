//Stand 30.09.2015
!function($, window, document, undefined){ "use strict";
    $.fn.fnagelKnob = function(userSettings){
        return this.each(function(){
            var
                $this = $(this),

                options = {
                    $plane: $this,
                    $rotatingParts: $this,
                    $wrigglingParts: $this,
                    rotateCW: true,
                    initialRotation: 0,
                    maxAngle: 350,
                    minAngle: 10,
                    intendThreshold: 0,
                    maxDuration: 1000,
                    minDuration: 200,
                    wrigglingStrength: 0.6,
                };

            //Insert public user vars and methods or overwrite existing ones
            $.extend(options, userSettings || {});

            var
                $plane          = options.$plane,
                $rotatingParts  = options.$rotatingParts,
                $wrigglingParts = options.$wrigglingParts,
                canDrag         = false,
                isDragging      = false,
                rotation        = options.initialRotation;

            function findClosestValue(newRotation){
                if(Math.abs(newRotation - options.maxAngle) <= Math.abs(newRotation - options.minAngle)){
                    return options.maxAngle;
                }else{
                    return options.minAngle;
                }
            }

            function wriggle(duration){
                var now = (new Date()).getTime(),
                    reference;

                reference = setInterval(function(){
                    if((new Date()).getTime() - now > duration){
                        clearInterval(reference);
                        $wrigglingParts.css({ '-webkit-transform': 'rotate(0deg)'});
                    }

                    var wriggleDeg = Math.round(options.wrigglingStrength*Math.random());

                    $wrigglingParts.css({ '-webkit-transform': 'rotate(' + wriggleDeg + 'deg)'});
                }, 1000 / 60);
            }

            function animateRotation(newRotation){
                var duration = (options.maxDuration - options.minDuration) / 360 * Math.abs(rotation - newRotation) + options.minDuration;

                $rotatingParts.css({
                    'webkit-transition': 'all ' + duration / 1000 + 's ease-in-out',
                    '-webkit-transform': 'rotate(' + newRotation + 'deg)'
                });

                wriggle(duration);
                rotation = newRotation;
            }

            function setRotation(newRotation){
                $rotatingParts.css({
                    'webkit-transition': 'none',
                    '-webkit-transform': 'rotate(' + newRotation + 'deg)'
                });
                rotation = newRotation;
            }

            function isRotatable(newRotation){
                if((newRotation > options.maxAngle || newRotation < options.minAngle) && options.rotateCW){
                    return false;
                }

                if((newRotation < options.maxAngle || newRotation > options.minAngle) && !options.rotateCW){
                    return false;
                }

                return true;
            }

            function getDegree(pageX, pageY){
                var
                    pos = $this.position(),
                    relPosX = pageX - (pos.left + 0.5*$this.width()),
                    relPosY = pageY - (pos.top + 0.5*$this.height()),
                    phi = Math.atan(relPosY / relPosX),
                    degree = phi * 180 / Math.PI;

                if(relPosX < 0 && relPosY > 0 || relPosX < 0 && relPosY <= 0){
                    degree = 180 + degree;
                }
                if(relPosX >= 0 && relPosY <= 0){
                    degree = 360 + degree;
                }

                if(!isRotatable(degree)){
                    if(isDragging){
                        degree = rotation;
                    }else{
                        degree = findClosestValue(degree);
                    }
                }

                return Math.round(degree);
            }

            $this.on('click', function(event){ var $this = $(this);
                var degree = getDegree(event.pageX, event.pageY);
                animateRotation(degree);
            });

            $this.on('mousedown', function(event){
                canDrag = true;
            });

            $this.on('mouseup mouseleave', function(event){
                canDrag = false;
                isDragging = false;
            });

            $this.on('mousemove', function(event){
                if(!canDrag){
                    return false;
                }
                var degree = getDegree(event.pageX, event.pageY);

                if(degree == rotation){
                    return false;
                }

                // chrome fix (ignore the first mousemove because one is fired anyway)
                if(!isDragging){
                    isDragging = true;
                    return false;
                }

                wriggle(1000 / 60);
                setRotation(degree);
            });
        });
    };

}(jQuery, window, document);
