!function($){ 'use strict';
    $.fn.ooMassCheckboxModifier = function(userSettings){
        return this.each(function (){
            var
                $this = $(this),
            	NAMESPACE = 'oo_mass_checkbox_modifier',
            	NAMESPACE_DELIMITER = '_',
            	NS = NAMESPACE + NAMESPACE_DELIMITER,

                /**
                 * initial options
                 */
                options = {
                    $checkboxes: $this.find('input[type="checkbox"]'),
                    dataActionKey: NS+'action',
                    selectedClass: NS+'selected'
                };

            //Insert public user vars and methods or overwrite existing ones
            $.extend(options, userSettings || {});

            /**
             * private vars
             */
            var
                $checkboxes = options.$checkboxes,
                selectedClass = options.selectedClass,

                checkboxActions = {
                	flood: function flood(){
                		$checkboxes.attr('checked',  true);
                	},

                	wipe: function wipe(){
                		$checkboxes.removeAttr('checked');
                	},

                	invert: function invert(){
                		$checkboxes.each(function(index, element){
                			var $checkbox = $(element);

                			if($checkbox.attr('checked')){
                				$checkbox.removeAttr('checked');
                			}else{
                				$checkbox.attr('checked', true);
                			}
                		});
                	}
                };


            /**
             * event listeners
             */
            $this.bind('click', function(event){
                try{
                	checkboxActions[$this.data(options.dataActionKey)]();
                }catch(e){
                	console.log(NAMESPACE+': action not found.');
                }
            });
        });
    };
}(jQuery);
