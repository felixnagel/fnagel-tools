//Stand 22.09.2014

!function($, window, document, undefined){
    "use strict";
    $.fn.FnagelZoom = function(userSettings){
        return this.each(function(){
            var
            $this = $(this),

            options = {
                $parent : $this.parent(),
                zoomEasingFunction : 'easeOutQuint',
                zoomDuration : 1000,
                zoomDelay : 0,
                callback : function(){}
            };

            //Insert public user vars and methods or overwrite existing ones
            $.extend(options, userSettings || {});

            /////////////
            // PRIVATE //
            /////////////
            var $parent = options.$parent,
                widthDefault = $this.width(),
                heightDefault = $this.height(),
                widthParent = $parent.width(),
                fPos = widthDefault / widthParent,
                zoomEasingFunction = options.zoomEasingFunction,
                zoomDuration = options.zoomDuration,
                zoomDelay = options.zoomDelay,
                callback = options.callback;



            ////////////
            // PUBLIC //
            ////////////
            $this.on('fnagel-zoom-zoom', function foo(event, params){
                var xTarget = params.posX,
                    yTarget = params.posY,
                    fZoom = params.zoom;

                $this.delay(params.zoomDelay ? params.zoomDelay : zoomDelay).animate(
                    {
                        'width' : widthDefault * fZoom,
                        'height' : heightDefault * fZoom,
                        'top' : -yTarget * fPos * fZoom + '%',
                        'left' : -xTarget * fPos * fZoom + '%'
                    },
                    params.zoomDuration ? params.zoomDuration : zoomDuration,
                    zoomEasingFunction,
                    function(){
                        if(params.callback){
                            params.callback();
                        }else{
                            callback();
                        }
                    }
                );
            });

            ////////////////////
            // INITIAL SCRIPT //
            ////////////////////
            $this.css({
                'position' : 'absolute'
            });
        });
    };

}(jQuery, window, document);
