!function($, window, document, undefined){ "use strict";
    $.fn.fnagelChrono1 = function(userSettings){
        return this.each(function(){
            var
            $this = $(this),

            options = {
                returnMilliseconds: false,
                returnSeconds: true,
                tickInterval: 1000
            };

            //Insert public user vars and methods or overwrite existing ones
            $.extend(options, userSettings || {});

            /**
             * private vars
             */
            var
                h = 0,
                intervalReference,
                m = 0,
                ms = 0,
                s = 0,
                tickInterval = options.tickInterval,
                ts;

            /**
             * private functions
             */
            var
                tick = function tick(){
                    var newTs = (new Date).getTime();
                    ms += newTs - ts;
                    ts = newTs;

                    while(ms > 999){
                        s++;
                        ms -= 1000;
                    }
                    while(s > 59){
                        m++;
                        s -= 60;
                    }
                    while(m > 59){
                        h++;
                        m -= 60;
                    }
                },

                pad = function pad(value, digits){
                    var stringValue = String(value);
                    while(stringValue.length < digits){
                        stringValue = "0" + stringValue;
                    }
                    return stringValue;
                },

                createOutput = function createOutput(){
                    var output = {
                        h: pad(h, 2),
                        i: pad(m, 2),
                        s: pad(s, 2),
                        part: pad(h, 2) + ':' + pad(m, 2),
                        full: pad(h, 2) + ':' + pad(m, 2) + ':' + pad(s, 2) + '.' + pad(ms, 3)
                    }

                    if(options.returnSeconds){
                        output.part += ':' + pad(s, 2);
                    }
                    if(options.returnMilliseconds){
                        output.part += ':' + pad(ms, 3);
                    }

                    return output;
                };


            /**
             * listeners
             */
            $this.on('fnagelChrono1-start', function(){
                if(typeof intervalReference === 'undefined'){
                    ts = (new Date).getTime();
                    intervalReference = setInterval(function(){
                        tick();
                        $this.trigger('fnagelChrono1-tick', createOutput());
                    }, tickInterval);
                }
            });

            $this.on('fnagelChrono1-stop', function(){
                intervalReference = clearInterval(intervalReference);
                tick();
                $this.trigger('fnagelChrono1-stopped', createOutput());
            });

            $this.on('fnagelChrono1-reset', function(){
                h = 0;
                m = 0;
                s = 0;
                ms = 0;
                $this.trigger('fnagelChrono1-tick', createOutput());
            });

            $this.on('fnagelChrono1-set', function(event, params){
                h = (params.h || 0);
                m = (params.m || 0);
                s = (params.s || 0);
                ms = (params.ms || 0);
                $this.trigger('fnagelChrono1-tick', createOutput());
            });

            /**
             * initial script
             */







        });
    };

}(jQuery, window, document);
