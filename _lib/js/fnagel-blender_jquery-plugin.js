//Stand 02.04.2015

!function($, window, document, undefined){
    "use strict";
    $.fn.fnagelBlender = function(userSettings){
        return this.each(function(){
            var
            $this = $(this),

            options = {
                initialIndex : 0,

                vectorFade : {in : null, out : null},

                $content : $this.find('>*'),

                fadeOpacity : {in : 1, out : 0},
                fadeAnimationDuration : {in : 400, out : 400},
                fadeAnimationDelay : {in : 0, out : 0},
                fadeAnimationEasingFunction : {in : 'easeInOutCubic', out : 'easeInOutCubic'},

                canLoop: false,
                autoShowIterationDuration: false,
                autoShowCallback: false,
            };

            //Insert public user vars and methods or overwrite existing ones
            $.extend(options, userSettings || {});

            /////////////
            // PRIVATE //
            /////////////
            var $content = options.$content,

                currentIndex = options.initialIndex,
                previousIndex,
                maximumIndex = $content.length - 1,

                canLoop = options.canLoop,

                canBlend = 2,

                autoShowCallback = options.autoShowCallback,
                autoShowIterationDuration = options.autoShowIterationDuration,

                animationIntervalReference;

            function processRequestedPosition(requestedPosition){
                //if requested index is beyond maximum
                if(requestedPosition > maximumIndex){
                    if(canLoop === true){
                        while(requestedPosition > maximumIndex){
                            requestedPosition -= maximumIndex;
                        }
                    }else{
                        requestedPosition = maximumIndex;
                    }
                }
                //if requested index is beyond minimum
                if(requestedPosition < 0){
                    if(canLoop === true){
                        while(requestedPosition < 0){
                            requestedPosition += maximumIndex;
                        }
                    }else{
                        requestedPosition = 0;
                    }
                }
                return requestedPosition;
            }

            function showCallback(callback){
                canBlend++;
                if(canBlend === 2){
                    //hide current content
                    $($content[previousIndex]).css({
                        'display' : 'none'
                    });

                    //reset auto show interval
                    if(autoShowIterationDuration !== false){
                        refreshAnimationInterval();
                    }

                    if(typeof callback === 'function'){
                        callback();
                    }

                    $this.trigger('fnagel-blender-blend-finished', currentIndex);
                }
            }

            //init animation interval
            function refreshAnimationInterval(){
                if(typeof animationIntervalReference !== 'undefined'){
                    clearInterval(animationIntervalReference);
                }
                animationIntervalReference = setInterval(function(){
                    $this.trigger('blender-show', {position: 1, relatively: true, callback: autoShowCallback});
                }, autoShowIterationDuration);
            }

            ////////////
            // PUBLIC //
            ////////////
            $this.on('fnagel-blender-blend', function blend(event, params){
                //process requested index
                if(params.relatively){
                    params.position += currentIndex;
                }
                var requestedIndex = processRequestedPosition(params.position);
                //kick if animation is still running or requested index is already displayed
                if(canBlend !== 2 || requestedIndex === currentIndex){
                    return false;
                }

                //disable this functionality until it is ready again
                canBlend = 0;

                //set: previous index == index of element to be blended out, current index == index of element to be blended in
                previousIndex = currentIndex;
                currentIndex = requestedIndex;

                //trigger event
                $this.trigger('fnagel-blender-blend-started', currentIndex);

                //get custom params
                var fadeAnimationDelay = params.fadeAnimationDelay ? params.fadeAnimationDelay : options.fadeAnimationDelay,
                    vectorFade = params.vectorFade ? params.vectorFade : options.vectorFade,
                    fadeAnimationDuration = params.fadeAnimationDuration ? paramsfadeAnimationDuration : options.fadeAnimationDuration,
                    fadeOpacity = params.fadeOpacity ? params.fadeOpacity : options.fadeOpacity,
                    blendCallback = params.callback ? params.callback : options.callback;


                //show new content
                $($content[currentIndex]).css({
                    'display' : 'block',
                    'z-index' : 11
                });

                //blend in requested content
                $this.trigger('blender-new-content-appeared', currentIndex);

                //set current content behind new content
                $($content[previousIndex]).css({
                    'z-index' : 10
                });

                //if opacity needs to be animated, use fadeTo to avoid bug in Chrome
                if(fadeOpacity.in != fadeOpacity.out){
                    //blend out currently shown content
                    $($content[previousIndex]).delay(fadeAnimationDelay.out).fadeTo(
                        fadeAnimationDuration.out,
                        fadeOpacity.out,
                        function(){
                            showCallback(blendCallback);
                        }
                    );

                    //blend in next shown content
                    $($content[currentIndex]).delay(fadeAnimationDelay.in).fadeTo(
                        fadeAnimationDuration.in,
                        fadeOpacity.in,
                        function(){
                            showCallback(blendCallback);
                        }
                    );
                }

                //if positioning needs to be animated for currently shown content
                if(vectorFade.out){
                    $($content[previousIndex]).delay(fadeAnimationDelay.out).animate(
                        {
                            'top' : vectorFade.out.y,
                            'left' : vectorFade.out.x
                        },
                        fadeAnimationDuration.out,
                        fadeAnimationEasingFunction.out,
                        function(){
                            if(vectorFade.in){
                                $($content[previousIndex]).css({
                                    'top': vectorFade.in.y,
                                    'left': vectorFade.in.x
                                });
                            }
                            showCallback(params.callback);
                        }
                    );
                }

                //if positioning needs to be animated for new content
                if(vectorFade.in){
                    $($content[currentIndex]).delay(fadeAnimationDelay.in).animate(
                        {
                            'top' : 0,
                            'left' : 0
                        },
                        fadeAnimationDuration.out,
                        fadeAnimationEasingFunction.out,
                        function(){
                            showCallback(params.callback);
                        }
                    );
                }
            });

            ////////////////////
            // INITIAL SCRIPT //
            ////////////////////
            //init css
            $this.css({
                'position': 'relative',
                'overflow': 'hidden'
            });

            $content.css({
                'opacity': options.fadeOpacity.out,
                'position': 'absolute',
                'display' : 'none'
            });

            if(options.vectorFade.in){
                $content.css({
                    'top': options.vectorFade.in.y,
                    'left': options.vectorFade.in.x
                });
            }else{
                $content.css({
                    'top': 0,
                    'left': 0
                });
            }

            $($content[currentIndex]).css({
                'display' : 'block',
                'opacity': options.fadeOpacity.in,
                'top': 0,
                'left': 0
            });

            //set auto show interval
            if(autoShowIterationDuration !== false){
                refreshAnimationInterval();
            }
        });
    };

}(jQuery, window, document);
