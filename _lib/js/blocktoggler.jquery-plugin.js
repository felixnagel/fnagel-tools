!function ($, window, document) {
    "use strict";
    $.fn.blocktoggler = function (userSettings) {
        return this.each(function () {
            var
                $this = $(this),
                /**
                 * initial options
                 */
                options = {
                    slaveSelector: '',
                    expandedClass: 'blocktoggler-expanded',
                    collapsedClass: 'blocktoggler-collapsed',
                    lsNamespace: 'blocktoggler',
                    storageId: 'blocktoggler'
                };

            //Insert public user vars and methods or overwrite existing ones
            $.extend(options, userSettings || {});

            /**
             * private vars
             */
            var
                storageId = options.storageId,
                $slave = $(options.slaveSelector),
                lsNamespace = options.lsNamespace,
                collapsedClass = options.collapsedClass,
                expandedClass = options.expandedClass,
                currentSlaveState,

                getCurrentSlaveState = function getCurrentSlaveState() {
                    return $slave.css('display');
                },

                getStoredSlaveState = function getStoredSlaveState(key) {
                    var finalKey = lsNamespace + '.' + storageId + '.' + key;
                    if (localStorage.getItem(finalKey) !== null) {
                        return JSON.parse(localStorage.getItem(finalKey));
                    } else {
                        return getCurrentSlaveState();
                    }
                },

                setStoredSlaveState = function setStoredSlaveState(key, value) {
                    var finalKey = lsNamespace + '.' + storageId + '.' + key;
                    localStorage.setItem(finalKey, JSON.stringify(value));
                },

                setCssClass = function setCssClass() {
                    if (currentSlaveState === 'block') {
                        $this.removeClass(collapsedClass);
                        $this.addClass(expandedClass);
                    } else {
                        $this.removeClass(expandedClass);
                        $this.addClass(collapsedClass);
                    }
                    return currentSlaveState;
                },

                toggleSlave = function toggleSlave() {
                    $slave.toggle({
                        duration: 0,
                        done: function () {
                            currentSlaveState = getCurrentSlaveState();
                            setStoredSlaveState('display', currentSlaveState);
                            setCssClass();
                        }
                    });
                };

            /**
             * event listeners
             */
            $this.on('click', function (event, param) {
                toggleSlave();
            });

            /**
             * initial script
             */
            currentSlaveState = getStoredSlaveState('display');
            $slave.css('display', currentSlaveState);
            setCssClass();
        });
    };

}(jQuery, window, document);
