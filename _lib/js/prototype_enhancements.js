!function(){ 'use strict'

// ############################################################################################
// ARRAY
//
// Das Extenden des Array-Prototyps führt dazu, dass die (ohnehin schon missbräuchliche) Verwendung
// von "for(var i in x)"-Schleifen zum Loopen durch ARRAYS einen Fehler verursacht.
// 
// Siehe: http://stackoverflow.com/questions/500504/why-is-using-for-in-with-array-iteration-such-a-bad-idea
// 
// Lösung:
// - Immer mit for(var i = 0; i < x.length; i++) loopen (sowieso Best Practice).
// - Falls doch eine For-in-Schleife NÖTIG ist, muss darin "x.hasownproperty(i)" abgefragt werden.

/**
 * indexOf Polyfill
 * Quelle: MDN
 */
// indexOf Polyfill (lte ie8)
if(!Array.prototype.indexOf){
  	Array.prototype.indexOf = function(elt /*, from*/){
    	var len = this.length >>> 0;
	    var from = Number(arguments[1]) || 0;
    	from = (from < 0) ? Math.ceil(from) : Math.floor(from);
    	if(from < 0){
    		from += len;
    	}
	    for(; from<len; from++){
      		if(from in this && this[from] === elt) return from;
    	}
    	return -1;
  	};
}

/**
 * Pusht einen Wert in den Array, falls nicht vorhanden.
 * Löscht den Wert aus dem Array, falls vorhanden.
 * @param 	mixed 	Wert
 * @return 	array 	THIS
 */
if(!Array.prototype.toggleValue){
	Array.prototype.toggleValue = function toggleValue(val){
		var index = this.indexOf(val);
		if(index === -1){
			this.push(val);
		}else{
			this.splice(index, 1);
		}
		return this;
	};
}


}();
