//Stand 26.05.2014


/*

put html like this:

<div id="fnagel-slider">
    <ul class="content-container">
        <li><img src="" alt="" /></li>
        <li><img src="" alt="" /></li>
        <li><img src="" alt="" /></li>
        <li><img src="" alt="" /></li>
    </ul>
    <div class="bottom-container">
        <div class="panel-container">
            <span id="#fnagel-slider-panel-previous"></span>
            <span class="panel index"></span>
            <span class="panel index"></span>
            <span class="panel index"></span>
            <span class="panel index"></span>
            <span id="fnagel-slider-panel-next"></span>
        </div>
    </div>
</div>

call like this:

var $fnagelSlider = $('#ministry-canvas #fnagel-slider'),
    $contentContainer = $('#ministry-canvas #fnagel-slider .content-container'),
    $buttonPrevious = $('#ministry-canvas #fnagel-slider-panel-previous'),
    $buttonNext = $('#ministry-canvas #fnagel-slider-panel-next');

//init fnagel-slider
$fnagelSlider.fnagelSlider1({
    $contentContainer: $contentContainer,
    //otherOptions
});

$buttonPrevious.on('click', function(event){
    $fnagelSlider.trigger('fnagel-slider-1-slide', {position: 2, relatively: true});
});
*/

!function($, window, document, undefined){
    "use strict";
    $.fn.fnagelSlider1 = function(userSettings){
        return this.each(function(){
            var
            $this = $(this),

            options = {
                $contentContainer: false,
                $indexPanels: false,
                $skipForwardPanel: false,
                $skipBackwardPanel: false,

                disableImageDragging: true,

                removeMargin: false,

                autoSlideIterationDuration: false,
                autoSlideNumberOfSkippedSites: 1,
                autoSlideCallback: false,

                canLoop: false,

                slideAnimationDuration: 400,
                slideAnimationDurationCommulativeFactor: false,
                slideAnimationEasingFunction: 'easeInOutCubic',

                initialSiteIndex: 0
            };

            //Insert public user vars and methods or overwrite existing ones
            $.extend(options, userSettings || {});

            /////////////
            // PRIVATE //
            /////////////
            var sliderWidth,

                $contentContainer = options.$contentContainer,

                contentContainerWidth,
                maximumContentContainerMargin,
                currentSiteIndex = options.initialSiteIndex,
                maximumSiteIndex,
                slidePositions,

                $content = $contentContainer.find('>*'),
                $contentImages = $content.find('img'),

                $indexPanels = options.$indexPanels,
                $skipForwardPanel = options.$skipForwardPanel,
                $skipBackwardPanel = options.$skipBackwardPanel,

                slideAnimationDuration = options.slideAnimationDuration,
                slideAnimationDurationCommulativeFactor = options.slideAnimationDurationCommulativeFactor,
                slideAnimationEasingFunction = options.slideAnimationEasingFunction,

                autoSlideIterationDuration = options.autoSlideIterationDuration,
                autoSlideNumberOfSkippedSites = options.autoSlideNumberOfSkippedSites,
                autoSlideCallback = options.autoSlideCallback,

                disableImageDragging = options.disableImageDragging,
                removeMargin = options.removeMargin,
                canLoop = options.canLoop,

                canAnimateContentContainer = true,
                animationIntervalReference,

                windowResizeEventTimeoutIdentifier;

            //init animation interval
            function refreshAnimationInterval(){
                if(typeof animationIntervalReference !== 'undefined'){
                    clearInterval(animationIntervalReference);
                }
                animationIntervalReference = setInterval(function(){
                    $this.trigger('fnagel-slider-1-slide', {position: autoSlideNumberOfSkippedSites, relatively: true, callback: autoSlideCallback});
                }, autoSlideIterationDuration);
            }

            function processRequestedPosition(requestedPosition){
                //if requested index is beyond maximum
                if(requestedPosition > maximumSiteIndex){
                    requestedPosition = maximumSiteIndex;
                    //only skip to beginning if current index is already at maximum
                    if(currentSiteIndex === maximumSiteIndex && canLoop === true){
                        requestedPosition = 0;
                    }
                }
                //if requested index is beyond minimum
                if(requestedPosition < 0){
                    requestedPosition = 0;
                    //only skip to end if current index is 0
                    if(currentSiteIndex === 0 && canLoop === true){
                        requestedPosition = maximumSiteIndex;
                    }
                }
                return requestedPosition;
            }

            function setCss(){
                if($indexPanels !== false){
                    $indexPanels.removeClass('active');
                    $($indexPanels[currentSiteIndex]).addClass('active');
                }
                if($skipBackwardPanel !== false){
                    if(isAtBeginning() === true){
                        $skipBackwardPanel.addClass('disabled');
                    }else{
                        $skipBackwardPanel.removeClass('disabled');
                    }
                }
                if($skipForwardPanel !== false){
                    if(isAtEnd() === true){
                        $skipForwardPanel.addClass('disabled');
                    }else{
                        $skipForwardPanel.removeClass('disabled');
                    }
                }
            }

            function setSlidePositions(){

            }

            function getCurrentSiteIndex(){
                return currentSiteIndex;
            }

            function isAtBeginning(){
                return currentSiteIndex === 0 ? true : false;
            }

            function isAtEnd(){
                return currentSiteIndex === maximumSiteIndex ? true : false;
            }

            ////////////
            // PUBLIC //
            ////////////
            //scrolls to requested index and executes given callback
            $this.on('fnagel-slider-1-slide', function slide(event, params){
                //process requested index
                if(params.relatively === true){
                    params.position += currentSiteIndex;
                }

                params.position = processRequestedPosition(params.position);

                //kick if animation still running or requested index is already shown
                if(params.position === currentSiteIndex || canAnimateContentContainer === false){
                    return false;
                }

                //calculate animation duration
                var slideAnimationDurationForThisTime = slideAnimationDuration,
                    deltaSiteIndex = Math.abs(params.position - currentSiteIndex);
                if(slideAnimationDurationCommulativeFactor !== false && deltaSiteIndex > 1){
                    slideAnimationDurationForThisTime = deltaSiteIndex * slideAnimationDuration * slideAnimationDurationCommulativeFactor;
                }

                //block functionality until process finished
                canAnimateContentContainer = false;

                //refresh animation interval if auto slide is activated
                if(autoSlideIterationDuration !== false){
                    refreshAnimationInterval();
                }

                //set current site index
                currentSiteIndex = params.position;

                //set css
                setCss();

                //animate content container
                $contentContainer.animate({
                    'margin-left': -slidePositions[currentSiteIndex]
                }, slideAnimationDurationForThisTime, slideAnimationEasingFunction, function(){
                    //execute callback
                    if(typeof params.callback === 'function'){
                        params.callback(currentSiteIndex);
                    }
                    //unblock this functionality
                    canAnimateContentContainer = true;
                    $this.trigger('fnagel-slider-1-slide-animation-has-finished', currentSiteIndex);
                });
                $this.trigger('fnagel-slider-1-slide-animation-has-started', currentSiteIndex);
                return currentSiteIndex;
            });

            ////////////////////
            // INITIAL SCRIPT //
            ////////////////////
            function init(){
                sliderWidth = $this.outerWidth();
                contentContainerWidth = 0;
                slidePositions = [];


                //calculate and set content container width and fill slide positions array
                $content.each(function(index, element){
                    contentContainerWidth += $(element).outerWidth(true);
                });

                //calculate and set maximun content container margin
                maximumContentContainerMargin = contentContainerWidth - sliderWidth;

                //calculate and set slide positions
                slidePositions.push(0);
                $content.each(function(index, element){
                    //if next position would not exceed maximum content container margin
                    if(slidePositions[index] < maximumContentContainerMargin){
                        //remove margin when option is set
                        if(removeMargin === true){
                            //remove left margin of current element
                            slidePositions[index] += parseInt($(element).css('margin-left'), 10);
                            var previousElement = $content[index - 1];
                            if(typeof previousElement !== 'undefined'){
                                //remove right margin of previous element
                                slidePositions[index] += parseInt($(previousElement).css('margin-right'), 10);
                            }
                        }
                        //add slide position
                        slidePositions.push(slidePositions[index] + $(element).outerWidth(true));
                    //if its the last one
                    }else{
                        slidePositions[slidePositions.length - 1] = maximumContentContainerMargin;
                        if(removeMargin === true){
                            slidePositions[slidePositions.length - 1] -= parseInt($(element).css('margin-right'), 10);
                        }
                        return false;
                    }
                });

                //set maximum site index according to number of slide positions
                maximumSiteIndex = slidePositions.length - 1;

                //init css
                setCss();

                $this.css({
                    'overflow'  : 'hidden'
                });

                $contentContainer.css({
                    'height'        : '100%',
                    'margin'        : 0,
                    'margin-left'   : -slidePositions[currentSiteIndex],
                    'overflow'      : 'hidden',
                    'padding'       : 0,
                    'width'         : contentContainerWidth
                });

                $content.css({
                    'display'   : 'block',
                    'float'     : 'left',
                    'height'    : '100%',
                    'position'  : 'relative'
                });

                //disable image dragging
                if(disableImageDragging === true){
                    $contentImages.on('dragstart', function(event){
                        event.preventDefault();
                    });
                }

                //init automatic animation
                if(autoSlideIterationDuration !== false){
                    refreshAnimationInterval();
                }
            }

            $(window).on('resize', function(){
                clearTimeout(windowResizeEventTimeoutIdentifier);
                windowResizeEventTimeoutIdentifier = setTimeout(function(){
                    init();
                }, 100);
            });

            init();

        });
    };

}(jQuery, window, document);
