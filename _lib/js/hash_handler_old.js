
// Simple Hash-Handler v0.3
// Felix Nagel, 08.02.2016

/*
Implementation:

[html + smarty]:
<a 
	href="#"
	data-oo_hash_handler="{['any_key_1' => 'any_value_1', ..., 'any_key_n' => 'any_value_n']|@json_encode|@htmlentities}" 	[any json]
	data-oo_hash_handler|action="toggle" 																					[set, unset, toggle]
	data-oo_hash_handler|listener="click" 																					[any event-listener]
>
	foo
</a>

[js + jquery]:
$(document).ready(function(){ 'use-strict';
	var $window = $(window);

	$window.on('oo_hash_handler|set@any_key_1', function(event, hashValue){
		// etwas machen, wenn sich der Wert von any_key_1 geändert hat
	});
	
	$window.on('oo_hash_handler|unset@show_favid', function(event, hashValue){
		// etwas machen, wenn any_key_1 gelöscht wurde
	});
	
	// API: window.oo.hash_handler
	window.oo.hash_handler.init();
	
	// hash per js manipulieren (mehr Funktionen siehe API)
	window.oo.hash_handler.set({
		foo_1: 'bar_1',
		...
		foo_n: 'bar_n'
	});
});
*/

$('document').ready(function(){ 'use strict';
	// APP CONSTANTS
	var
		PRIMARY_NAMESPACE = 'oo',
		NAMESPACE_DELIMITER = '_',
		NAMESPACE = 'hash_handler',
		
		FNS = PRIMARY_NAMESPACE + NAMESPACE_DELIMITER + NAMESPACE,
		
		DATA_ID_ACTION_SET = 'set',
		DATA_ID_ACTION_UNSET = 'unset',
		DATA_ID_ACTION_TOGGLE = 'toggle',
		DATA_ID_ACTION_KEY = 'action',
		DATA_ID_EVENT_KEY = 'listener';
	
	// SETTINGS
	var
		s = {
			delimiter1: '|',
			delimiter2: '@',
			defaultAction: DATA_ID_ACTION_SET,
			defaultListener: 'click',
			autoRefresh: true
		};

		
	// VARS
	var
		_$window = $(window),
		_hash = '',
		_hashJson = {};

		
	// METHODS
	var
		m = {
			/**
			 * Entfernt gesamte Hash.
			 */
			clear_hash: function clear_hash(){
				location.hash = '';
			},

			/**
			 * Init-Funktion. Muss vom User manuell ausgeführt werden, um Timing-Probleme zu
			 * vermeiden.
			 */
			init: function init(){
				var $triggeringElements = $('*[data-'+FNS+']');

				$triggeringElements.each(function(){ var $this = $(this);
					// Eventlistener auslesen.
					var specifiedEvent = $this.data(FNS+s.delimiter1+DATA_ID_EVENT_KEY);
					if(!specifiedEvent){
						specifiedEvent = s.defaultListener;
					}

					// Aktion auslesen.
					var specifiedActionType = $this.data(FNS+s.delimiter1+DATA_ID_ACTION_KEY);
					if(!specifiedActionType || !API[specifiedActionType]){
						specifiedActionType = s.defaultAction;
					}
					
					// Event-Listener Definieren.
					$this.on(specifiedEvent, function(){
						API[specifiedActionType]($this.data(FNS));
					});
				});

				if(s.autoRefresh){
					_$window.on('hashchange', m.on_hashchange);
					m.on_hashchange();
				}
			},

			/**
			 * Mappt auf set und unset, je nach Vorhandensein des jeweiligen Keys.
			 * @param	JSON	json	Json mit den entsprechenden Daten.
			 */
			toggle_hash_params_json: function toggle_hash_params_json(json){
				var hashJson = m.read_hash_json();
				for(var key in json){
					if(hashJson[key] === undefined){
						hashJson[key] = json[key];
					}else{
						delete hashJson[key];
					}
				}
				m.replace_hash_json(hashJson);
			},

			/**
			 * Klont einen JSON (zerstört Referenz)
			 * @param 	JSON	json	Zu klonender Json.
			 * @return  JSON			Geklonter Json (keine Referenz).
			 */
			clone_json: function clone_json(json){
				return JSON.parse(JSON.stringify(json));
			},

			/**
			 * Ersetzt die gesamte Hash mit einem beliebigen String.
			 * @param 	string	newHash	Der zu setzende Hash-String.
			 */
			replace_hash: function replace_hash(newHash){
				window.location.hash = newHash;
			},

			/**
			 * Ersetzt die Hash mit einem beliebigen Json. Ein leerer Json wird in einen Leerstring
			 * umgewandelt.
			 * @param 	JSON	json	Der als Hash zu setzende Json.
			 */
			replace_hash_json: function replace_hash_json(json){
				var newHash = JSON.stringify(json);
				if(newHash == '{}'){
					newHash = '';
				}
				m.replace_hash(newHash);
			},

			/**
			 * Entfernt eine beliebige Anzahl Keys aus dem Hash-Json. Ein einzelner Key kann als
			 * String übergeben werden, mehrere Keys können als Array übergeben werden.
			 * @param 	mixed 	keys 	Die zu entfernenden Keys.
			 */
			unset_hash_json: function unset_hash_json(keys){
				if(typeof keys === 'string'){
					keys = [keys];
				}
				var hashJson = m.read_hash_json();
				for(var i = 0; i < keys.length; i++){
					delete hashJson[keys[i]];
				}
				m.replace_hash_json(hashJson);
			},

			/**
			 * Fügt einen Json in den Hash-Json ein (merge).
			 * @param 	JSON 	json 	Der einzufügende Json.
			 */
			modify_hash_json: function modify_hash_json(json){
				var hashJson = m.read_hash_json();
				for(var key in json){
					hashJson[key] = json[key];
				}
				m.replace_hash_json(hashJson);
			},
			
			/**
			 * Liest die Hash aus.
			 * @return string  Der ausgelesene Hash-String.
			 */
			read_hash: function read_hash(){
				// decodeURIComponent() weil: FF encoded automatisch, der Rest nicht
				return decodeURIComponent(window.location.hash.substr(1)); 
			},

			/**
			 * Liest die Hash aus und versucht den String in einen Json umzuwandeln. Bei
			 * Misslingen wird ein leerer Json zurückgegeben.
			 * @return	JSON 	Der Hash-Json.
			 */
			read_hash_json: function read_hash_json(){
				try{
					// decodeURIComponent() weil: FF encoded automatisch, der Rest nicht
					return JSON.parse(decodeURIComponent(window.location.hash.substr(1)));
				}catch(e){
					return {};
				}
			},
			
			/**
			 * Prüft, für welche Keys im Hash-Json die Values geändert wurden.
			 * @param 	JSON 	newData 	Der neue Hash-Json
			 * @return 	array 				Ein Array, der alle geänderten Keys beinhaltet.
			 */
			get_changed_json_keys: function get_changed_json_keys(newData){
				var changedKeys = [];
				for(var key in newData){
					if(_hashJson[key] !== undefined){
						if(JSON.stringify(_hashJson[key]) !== JSON.stringify(newData[key])){
							changedKeys.push(key);
						}
					}else{
						changedKeys.push(key);
					}
				}
				return changedKeys;
			},
			
			/**
			 * Prüft, welche Keys im Hash-Json nicht mehr existieren.
			 * @param 	JSON 	newData 	Der neue Hash-Json
			 * @return 	array 				Ein Array, der alle gelöschten Keys beinhaltet.
			 */
			get_removed_json_keys: function get_removed_json_keys(newData){
				var changedKeys = [];
				for(var key in _hashJson){
					if(newData[key] === undefined){
						changedKeys.push(key);
					}
				}
				return changedKeys;
			},
			
			on_hashchange: function on_hashchange(event){
				var newHash = m.read_hash();
				if(newHash !== _hash){
					var 
						sEventName = FNS + s.delimiter1 + DATA_ID_ACTION_SET,
						eventData = {value: newHash};
					_$window.trigger(sEventName, eventData);					
					_hash = newHash;
				}

				var
					newHashJson = m.read_hash_json(),
					changedKeys = m.get_changed_json_keys(newHashJson),
					removedKeys = m.get_removed_json_keys(newHashJson);

				for(var i = 0; i < changedKeys.length; i++){
					_hashJson[changedKeys[i]] = newHashJson[changedKeys[i]];
					var 
						sEventName = FNS+s.delimiter1+DATA_ID_ACTION_SET+s.delimiter2
							+changedKeys[i],
						eventData = {
							key: changedKeys[i],
							value: m.clone_json(_hashJson[changedKeys[i]])
						};
					
					_$window.trigger(sEventName, eventData);
				}
				
				for(var i = 0; i < removedKeys.length; i++){
					var 
						sEventName = FNS+s.delimiter1+DATA_ID_ACTION_UNSET+s.delimiter2
							+changedKeys[i],
						eventData = {key: removedKeys[i]};
					
					_$window.trigger(sEventName, eventData);
					delete _hashJson[removedKeys[i]];
				}
			}
		};
	
	
	// API
	var 
		API = {
			settings: s,
			init: m.init,
			refresh: m.on_hashchange,
			
			get: m.read_hash,
			set: m.replace_hash,
			clear: m.clear_hash,
			
			json_get: m.read_hash_json,
			json_set: m.replace_hash_json,
			json_toggle: m.toggle_hash_params_json,
			json_unset: m.unset_hash_json			
		},

		API_CONTAINER = {};

	API_CONTAINER[PRIMARY_NAMESPACE] = {};
	API_CONTAINER[PRIMARY_NAMESPACE][NAMESPACE] = API;
	
	// API implementieren
	$.extend(true, window, API_CONTAINER);

	// Ready-Event werfen
	$(window).trigger(FNS+'@ready');
});
