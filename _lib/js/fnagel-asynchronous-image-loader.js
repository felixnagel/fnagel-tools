// 09.04.15
!function($){
	'use strict';
	$.FnagelAsynchronousImageLoader = function(userSettings){
		var	self = this;
		/**
		 * set default settings
		 */
		self.settings = {
			$images				: [],
			urls				: [],
			onImageLoaded		: function(event, process, queueItem, queue){},
			onQueueCompleted	: function(process, queue){}
		};

		$.extend(self.settings, userSettings || {});

		/**
		 * add images to queue
		 */
		$(self.settings.$images).each(function(index){ var $this = $(this);
			self.addItem($this.data('src'), $this);
		});

		/**
		 * add urls to queue
		 */
		for(var key in self.settings.urls){
			self.addItem(self.settings.urls[key]);
		}
	};

	/**
	 * define class prototype
	 */
	$.FnagelAsynchronousImageLoader.prototype = {
		/**
		 * loading queue
		 * @type 	array	array of jsons
		 */
		queue : [],

		/**
		 * adds a queue item at the end of the queue
		 * @param 	string		src				image src
		 * @param  	object 		$image			jQuery selection of an img (optional)
		 * @param  	function	onImageLoaded	callback function to be executed onload
		 */
		addItem : function addItem(src, $image, onImageLoaded){ var self = this;
			onImageLoaded = typeof onImageLoaded === 'function' ? onImageLoaded : self.settings.onImageLoaded;

			var queueItem = {
					$image 				: $image,
					loadingReference 	: new Image(),
					src 				: src,

					startedLoading 		: false,
					finishedLoading 	: false,
					loadingTime 		: false,

					onImageLoaded 		: onImageLoaded
				};

			self.queue.push(queueItem);

			return queueItem;
		},

		/**
		 * get loading progress
		 * @return	json	{itemsCompleted, itemsTotal, progressPercent, progressRelative}
		 */
		getProgress : function getProgress(){ var self = this;
			/**
			 * count completed items
			 */
			var
				itemsCompleted = 0,
				itemsTotal     = self.queue.length;
			for(var key in self.queue){
				var queueItem = self.queue[key];
				if(queueItem.finishedLoading){
					itemsCompleted++;
				}
			}

			/**
			 * calculate relative progress
			 */
			var
				progressPercent  = itemsCompleted / itemsTotal * 100,
				progressRelative = itemsCompleted / itemsTotal;

			/**
			 * get earliest loading start
			 */
			var minStartedLoading = Number.POSITIVE_INFINITY;
			for(var key in self.queue){
				var queueItem = self.queue[key];
				if(queueItem.startedLoading < minStartedLoading){
					minStartedLoading = queueItem.startedLoading;
				}
			}

			/**
			 * get latest loading finished
			 */
			var maxFinishedLoading = 0;
			for(var key in self.queue){
				var queueItem = self.queue[key];
				if(queueItem.finishedLoading > maxFinishedLoading){
					maxFinishedLoading = queueItem.finishedLoading;
				}
			}

			/**
			 * calculate loading duration
			 */
			var loadingDuration = (maxFinishedLoading - minStartedLoading) / 1000;

			return {
				itemsTotal 			: itemsTotal,
				itemsCompleted 		: itemsCompleted,

				progressPercent		: progressPercent,
				progressRelative 	: progressRelative,

				loadingDuration 	: loadingDuration
			};
		},

		/**
		 * load next item of queue, recursive
		 */
		start : function start(){ var self = this;
			/**
			 * search for next queue item with status 'queued',
			 * start to load it and define its callbacks
			 * also trigger events for load start and complete
			 */
			for(var key in self.queue){
				if(!self.queue[key].finishedLoading){
					/**
					 * start loading
					 */
					self.queue[key].loadingReference.src = self.queue[key].src;
					self.queue[key].startedLoading = new Date().getTime();

					// trigger event FnagelAsynchronousImageLoader_image_started_loading
					$(self).trigger('FnagelAsynchronousImageLoader_image_started_loading', {
						progress 	: self.getProgress(),
						queue 		: self.queue,
						queueItem 	: self.queue[key]
					});

					/**
					 * define onload event and callback,
					 * create new scope to hold key
					 */
					!function(key){
						self.queue[key].loadingReference.onload = function(event){
							 // set this queue item's finished loading timestamp
							self.queue[key].finishedLoading = new Date().getTime();
							// claculate this queue item's loading time in milliseconds
							self.queue[key].loadingTime = (self.queue[key].finishedLoading - self.queue[key].startedLoading) / 1000;

							// get progress
							var progress = self.getProgress();

							/**
							 * trigger event FnagelAsynchronousImageLoader_image_loaded
							 */
							$(self).trigger('FnagelAsynchronousImageLoader_image_loaded', {
								progress 	: self.getProgress(),
								queue 		: self.queue,
								queueItem 	: self.queue[key]
							});

							/**
							 * exec callback, fallback to default callback if no queue item callback was specified
							 */
							self.queue[key].onImageLoaded(event, progress, self.queue[key], self.queue);

							/**
							 * handle queue completed event
							 */
							if(progress.progressRelative === 1){
								$(self).trigger('FnagelAsynchronousImageLoader_queue_completed', self.queue);
								self.settings.onQueueCompleted(progress, self.queue);
							}
						};
					}(key);
				}
			}
		},

		/**
		 * stop loading images, uncompleted progress will be lost
		 */
		stop : function stop(){ var self = this;
			for(var key in self.queue){
				var	item = self.queue[key];
				item.loadingReference.src = '';
			}
		}
	};
}(jQuery);
