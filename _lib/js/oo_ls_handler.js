!function(){
	// APP CONSTANTS
	var
		PRIMARY_NAMESPACE = 'oo',
		NAMESPACE_DELIMITER = '_',
		NAMESPACE = 'ls_handler';
		
	// SETTINGS
	var
		s = {};
		
	// VARS
	var
		_fullNamespace = PRIMARY_NAMESPACE + NAMESPACE_DELIMITER + NAMESPACE,
		_data = {},
		ls = localStorage;
		
	// METHODS
	var
		// PUBLIC
		/**
		 * Init-Funktion. Muss vom User manuell ausgeführt werden, um Timing-Probleme zu vermeiden.
		 */
		set = function set(key, value){
			_data[key] = value;
			m.store();
		},

		unset = function unset(key){
			delete _data[key];
			m.store();
		},
		
		get = function get(key){
			m.fetch_data();
			
			return _data[key];
		},

		clear = function clear(){
			_data = {};
			m.store();
		},

		// PRIVATE
		m = {
			store: function store(){
				try{
					ls.setItem(_fullNamespace, JSON.stringify(_data));
				}catch(e){
					console.log(_fullNamespace + ': Could not write into Local Storage!');
				}
			},

			fetch_data: function fetch_data(){
				try{
					_data = JSON.parse(ls.getItem(_fullNamespace)) || {};
				}catch(e){
					console.log(_fullNamespace + ': Could not read Local Storage!');
				}
			},

			init: function init(){
				m.fetch_data();
			}
		};
	

	m.init();


	// API
	var 
		api = {};
		api[PRIMARY_NAMESPACE] = {};
		api[PRIMARY_NAMESPACE][NAMESPACE] = {
			clear: clear,
			get: get,
			set: set,
			unset: unset
		};
	
	// API implementieren
	$.extend(true, window, api);
}();
