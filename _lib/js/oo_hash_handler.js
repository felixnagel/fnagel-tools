'use strict';

// Hash-Handler v2.0
// Felix Nagel, 11.05.2016

// Singelton init
var 
	OoHashHandler = (function(){
		var instance;
		return {
			get_instance: function get_instance(settings){
				if(!instance){
					instance = new _OoHashHandler(settings);
				}
				return instance;
			}
		};
	})();

// Hash-Handler-Object
function _OoHashHandler(settings){
	// ############################################################################################
	// Private
	var
		self = this,
		p = {
			// Dieser JSON beinhaltet alle Daten, die in der Hash stehen. Bei jedem Hashchange
			// wird der data-JSON and die Hash angepasst.
			data: {},
			// Dieser JSON beinhaltet alle Daten, die in der Hash stehen. Zusätzlich jedoch
			// beinhaltet er alle die Daten, die noch in die Hash geschrieben werden müssen. Bei
			// jedem Hashchange wird der newData-JSON and die Hash angepasst.
			newData: {},

			timeoutReference: null,

			/**
			 * Liest die Hash aus.
			 * @return	string	ausgelesene Hash
			 */
			read_hash: function read_hash(){
				// decodeURIComponent() weil: FF encoded automatisch, der Rest nicht
				return decodeURIComponent(window.location.hash.substr(1));
			},

			/**
			 * Liest die Hash aus und parst sie als JSON.
			 * @return	JSON 	ausgelesene Hash
			 */	
			read_hash_json: function read_hash_json(){
				try{
					var hashJson = JSON.parse(p.read_hash());
				}catch(e){
					var hashJson = {};
				}
				return hashJson;
			},

			/**
			 * Klont einen JSON. Die Referenz geht dabei verloren.
			 * @param	JSON	zu klonender JSON
			 * @return 	JSON 	geklonter JSON
			 */
			clone_json: function clone_json(json){
				return JSON.parse(JSON.stringify(json));
			},

			/**
			 * Handler für Hashchange-Event. Die aktuelle Hash wird ausgelesen und deren Daten mit
			 * den derzeit hinterlegten Daten abgeglichen. Bei Unterschieden wird ein Event
			 * gefeuert und die Daten werden synchronisiert.
			 */
			onhashchange: function onhashchange(){
				var newHashJson = p.read_hash_json();
				
				// Für jeden Key des JSON:
				// Vergleicht die Hash mit den hinterlegten Daten.
				for(var key in newHashJson){
					// Falls Datum noch nicht vorhanden oder geändert:
					if(
						p.data[key] === undefined
						||
						JSON.stringify(p.data[key]) != JSON.stringify(newHashJson[key])
					){
						// Hinterlegt die neuen Daten unter data-JSON und newData-JSON.
						p.data[key] = newHashJson[key];
						p.newData[key] = newHashJson[key];
						// Feuert Event.
						self.trigger(self.settings.key_changed, key);
					}
				}
				// Für jeden Key des data-JSON:
				// Überprüft, ob Daten verschwunden sind.
				for(var key in p.data){
					if(newHashJson[key] === undefined){
						// Feuert Event.
						self.trigger(self.settings.key_changed, key, newHashJson[key]);
						// Löscht hinterlegtes Datum.
						delete p.data[key];
					}
				}
			}		
		};

	// ############################################################################################
	// Public
	self.settings = {
		delimiter: '|',
		key_changed: 'changed',
	};
	$.extend(self.settings, settings || {}, true);
	
	/**
	 * Getter für data-JSON. Param key kann weggelassen werden, um die gesamten Daten zu bekommen.
	 * @param 	string 	key 	Key (optional)
	 * @return 	mixed 			Wert
	 */
	self.get = function get(key){
		return key ? p.data[key] : p.data;
	};

	/**
	 * Fügt Daten hinzu, welche bei der nächsten Ausführung von write() in die Hash geschrieben
	 * werden.
	 * 
	 * Achtung:
	 * Falls zwischen den Aufrufen von prepare() und write() ein Hashchange triggert, können die
	 * mit prepare() gesetzten Daten verloren gehen.
	 * 
	 * @param	string	key		Key
	 * @param 	mixed	value	Value
	 * @return 	Object 			this (für Chaining)
	 */
	self.prepare = function prepare(key, value){
		p.newData[key] = value;
		return self;
	};

	/**
	 * Schreibt die gepufferten Daten in die Hash. Optional kann auch ein Key-Value-Paar mit
	 * angegeben werden, welches zusätzlich vorher noch gesetzt wird.
	 *
	 * @param	string	key		Key (optional)
	 * @param 	mixed	value	Value (optional)
	 * @return 	Object 			this (für Chaining)
	 */
	self.write = function write(key, data){
		if(key !== undefined){
			self.prepare(key, data);
		}

		// Achtung:
		// Wenn direkt hintereinander mehrfach die Hash geändert wird, kommt der Browser nicht mehr
		// mit. Daher setTimeout().
		clearTimeout(p.timeoutReference);
		p.timeoutReference = setTimeout(function(){
			window.location.hash = JSON.stringify(p.newData);
		}, 5);
		return self;
	};

	/**
	 * Triggert ein Event auf sich selbst. Dieses Event wird für jedes in der Hash geändertes
	 * Key-Value-Paar automatisch getriggert. Es kann aber auch manuell getriggert werden.
	 * Der Name des Events besteht immer aus [NAME][DELIMITER][KEY]. Es wird des jeweilig
	 * betroffene Key-Value-Paar mitgeschickt.
	 * 
	 * @param 	string 	name 	Name des Events.
	 * @param  	string 	key  	Key
	 * @return 	Object 			this (für Chaining)
	 */
	self.trigger = function trigger(name, key){
		$(self).trigger(name+self.settings.delimiter+key, {
			key: key,
			value: p.clone_json(p.data[key])
		});
		return self;
	};

	
	// ############################################################################################
	// Init
	// Einmalig Daten auslesen und setzen.
	p.data = p.read_hash_json();
	p.newData = p.read_hash_json();
	// Auf Hashchange-Event hören.
	$(window).on('hashchange', p.onhashchange);
}
