// 08.04.15
!function($){
	'use strict';
	$.FnagelSynchronousImageLoader = function(userSettings){
		var	self = this;

		/**
		 * set default settings
		 */
		self.settings = {
			$images 			: [],
			urls 				: [],
			onImageLoaded 		: function(event, process, queueItem, queue){},
			onQueueCompleted 	: function(process, queue){},
		};

		$.extend(self.settings, userSettings || {});

		/**
		 * add images to queue
		 */
		$(self.settings.$images).each(function(index){
			var	$image = $(this);
			self.appendItem($image.data('src'), $image);
		});

		/**
		 * add urls to queue
		 */
		for(var key in self.settings.urls){
			self.appendItem(self.urls[key]);
		}
	};

	/**
	 * define class prototype
	 */
	$.FnagelSynchronousImageLoader.prototype = {
		/**
		 * load queue
		 * @type 	array	array of jsons
		 */
		queue : [],

		/**
		 * creates a queue item data json
		 * @param 	string		src				image src
		 * @param  	object 		$image			jQuery selection of an img (optional)
		 * @param  	function	onImageLoaded	callback function to be executed onload
		 * @return 	json						created queue item data json
		 */
		prepareQueueItem : function prepareQueueItem(src, $image, onImageLoaded){ var self = this;
			onImageLoaded = typeof onImageLoaded === 'function' ? onImageLoaded : self.settings.onImageLoaded;
			var queueItem = {
					$image 				: $image,
					loadingReference 	: new Image(),
					src 				: src,

					startedLoading 		: false,
					finishedLoading 	: false,
					loadingTime 		: false,

					onImageLoaded 		: onImageLoaded
				};

			return queueItem;
		},

		/**
		 * adds a queue item at the start of the queue
		 * @param 	string		src				image src
		 * @param  	object 		$image			jQuery selection of an img (optional)
		 * @param  	function	onImageLoaded	callback function to be executed onload
		 */
		prependItem : function prependItem(src, $image, onImageLoaded){ var self = this;
			self.queue.unshift(self.prepareQueueItem(src, $image, onImageLoaded));
		},

		/**
		 * adds a queue item at the end of the queue
		 * @param 	string		src				image src
		 * @param  	object 		$image			jQuery selection of an img (optional)
		 * @param  	function	onImageLoaded	callback function to be executed onload
		 */
		appendItem : function appendItem(src, $image, onImageLoaded){ var self = this;
			self.queue.push(self.prepareQueueItem(src, $image, onImageLoaded));
		},

		/**
		 * get loading progress
		 * @return	json	{itemsCompleted, itemsTotal, progressPercent, progressRelative}
		 */
		getProgress : function getProgress(){ var self = this;
			/**
			 * count completed items
			 */
			var
				itemsCompleted = 0,
				itemsTotal     = self.queue.length;
			for(var key in self.queue){
				var queueItem = self.queue[key];
				if(queueItem.finishedLoading){
					itemsCompleted++;
				}
			}

			/**
			 * calculate relative progress
			 */
			var
				progressPercent  = itemsCompleted / itemsTotal * 100,
				progressRelative = itemsCompleted / itemsTotal;

			/**
			 * sum loading duration
			 */
			var loadingDuration = 0;
			for(var key in self.queue){
				var queueItem = self.queue[key];
				if(queueItem.finishedLoading){
					itemsCompleted++;
					loadingDuration += queueItem.loadingTime;
				}
			}

			return {
				itemsTotal 			: itemsTotal,
				itemsCompleted 		: itemsCompleted,

				progressPercent 	: progressPercent,
				progressRelative	: progressRelative,

				loadingDuration 	: loadingDuration
			};
		},

		/**
		 * load next item of queue, recursive
		 */
		start : function start(){ var self = this;
			/**
			 * search for next queue item with status 'queued',
			 * start to load it and define its callbacks
			 * also trigger events for load start and complete
			 */
			for(var key in self.queue){
				var queueItem = self.queue[key];
				if(!queueItem.startedLoading){
					/**
					 * start loading
					 */
					queueItem.loadingReference.src = queueItem.src;
					self.queue[key].startedLoading = new Date().getTime();

					// trigger event FnagelImageLoader_image_started_loading
					$(self).trigger('FnagelImageLoader_image_started_loading', {
						progress	: self.getProgress(),
						queue 		: self.queue,
						queueItem	: queueItem
					});

					/**
					 * define onload event and callback
					 */
					queueItem.loadingReference.onload = function(event){
						 // set this queue item's finished loading timestamp
						self.queue[key].finishedLoading = new Date().getTime();
						// claculate this queue item's loading time in milliseconds
						self.queue[key].loadingTime = (self.queue[key].finishedLoading - self.queue[key].startedLoading) / 1000;

						// get progress
						var progress = self.getProgress();

						/**
						 * trigger event FnagelImageLoader_image_loaded
						 */
						$(self).trigger('FnagelImageLoader_image_loaded', {
							progress	: self.getProgress(),
							queue 		: self.queue,
							queueItem 	: queueItem
						});

						/**
						 * exec callback, fallback to default callback if no queue item callback was specified
						 */
						queueItem.onImageLoaded(event, progress, queueItem, self.queue);

						/**
						 * handle queue completed event
						 */
						if(progress.progressRelative === 1){
							$(self).trigger('FnagelImageLoader_queue_completed', self.queue);
							self.settings.onQueueCompleted(progress, self.queue);
						}

						// trigger recursion
						self.start();
					};

					// break this loop to only trigger one load at once
					return false;
				}
			}
		},

		/**
		 * stop loading images, uncompleted progress will be lost
		 */
		stop : function stop(){ var self = this;
			for(var key in self.queue){
				var	item = self.queue[key];
				item.loadingReference.src = '';
			}
		},
	};
}(jQuery);
