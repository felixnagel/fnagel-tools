$('document').ready(function(){ 'use strict';
	var
		PRIMARY_NAMESPACE = 'oo',
		NAMESPACE = 'simple_date_parser';

	var
		// PUBLIC
		parse_dmYHis = function parse_dmYHis(timeString){
			var	date = m.extract_date_from_string(timeString),
				dateString = time_to_YmdHis(date);

			return {
				date: dateString,
				ts: Date.parse(dateString)
			};
		},

		time_to_YmdHis = function time_to_YmdHis(date){
			date = m.normalize_time(date);
			return date.year + '-' + date.month + '-' + date.day + ' ' + date.hour + ':'
				+ date.minute + ':' + date.second;
		},

		time_to_dmYHis = function time_to_dmYHis(date){
			date = m.normalize_time(date);
			return date.day + '.' + date.month + '.' + date.year + ' ' + date.hour + ':'
				+ date.minute + ':' + date.second;
		},

		// PRIVATE
		m = {
			l_0: function l_0(numString){
				if(parseInt(numString, 10) < 10){
					return '0' + parseInt(numString, 10);
				}else{
					return parseInt(numString, 10);
				}
			},

			get_time_defaults: function get_time_defaults(){
				var date = new Date();
				return {
					year: date.getFullYear(),
					month: date.getMonth() + 1,
					day: date.getDate(),
					hour: date.getHours(),
					minute: date.getMinutes(),
					second: date.getSeconds()
				};
			},

			extract_date_from_string: function extract_date_from_string(timeString){
				var
					matches = timeString.match(/((\d{1,2})\.*){0,1}\s*((\d{1,2})\.*){0,1}\s*((\d{4})\s*){0,1}((\d{1,2})\:*){0,1}((\d{1,2})\:*){0,1}((\d{1,2})){0,1}/),
					defaultDate = m.get_time_defaults(),
					parsedDate = {
						year: matches[6],
						month: matches[4],
						day: matches[2],
						hour: matches[8],
						minute: matches[10],
						second: matches[12]
					};

				for(var key in parsedDate){
					if(parsedDate[key] === undefined){
						parsedDate[key] = defaultDate[key];
					}
				}

				return new Date(parsedDate.year, parsedDate.month-1, parsedDate.day,
					parsedDate.hour, parsedDate.minute, parsedDate.second);
			},

			normalize_time: function normalize_time(date){
				return {
					year: (date.getFullYear()),
					month: m.l_0(date.getMonth() + 1),
					day: m.l_0(date.getDate()),
					hour: m.l_0(date.getHours()),
					minute: m.l_0(date.getMinutes()),
					second: m.l_0(date.getSeconds())
				};
			}
		};


	// API
	var
		api = {};
		api[PRIMARY_NAMESPACE] = {};
		api[PRIMARY_NAMESPACE][NAMESPACE] = {
			parse_dmYHis: parse_dmYHis,
			time_to_dmYHis: time_to_dmYHis,
			time_to_YmdHis: time_to_YmdHis
		};

	// API implementieren
	$.extend(true, window, api);
});
