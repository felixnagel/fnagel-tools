<?php
namespace app\custom;
use Morrow\Debug;
use Morrow\Factory;

/**
* SETUP:
*	To use NodePathFinder, pass the nodeh ierarchy array to the constructor.
*	The nodeh ierarchy array must be like the following:
*
*		$myBreadcrumbHierarchy = array(
*			'root' => array(
*				'level1a' => array(
*					'level2a',
*					'level2b',
*				),
*				'level1b',
*			),
*		);
*
*	In words, you have to create a multi-dimensional array with the following entries:
*		1. dimension: root identifier
*		nth. dimension: any identifier
*/
class NodePathFinder{
	private $_nodeHierarchy = null;

	/**
	 * Constructor
	 *
	 * @param array $nodeHierarchy the nodeHierarchy array
	 *
	 * @return null
	 */	function __construct($nodeHierarchy){
		$this->_nodeHierarchy = $nodeHierarchy;
	}

	/**
	* Get generated node path
	*
	* @param $nodeIdentifier the name of current node
	*
	* @return array the generated node path
	*/
	public function getPath($nodeIdentifier){
		//get the path to this node according to hierarchy
		return $this->_getNodePath($nodeIdentifier, $this->_nodeHierarchy);
	}

	/**
	* generate the node node path
	*
	* @param string $needle the node name where the path leads to
	* @param array $haystack the array to search in
	*
	* @return array $result the resulting node path
	*/
	private function _getNodePath($needle, $haystack){
		$result = array();
		foreach($haystack as $key => $value){
			$identifier = is_array($value) ? $key : $value;

			if($needle == $identifier){
				return array($identifier);
			}

			if(is_array($value)){
				$result = $this->_getNodePath($needle, $value);
				if($result){
					array_unshift($result, $identifier);
					break;
				}
			}
		}
		return $result;
	}
}
