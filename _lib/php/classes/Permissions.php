<?php
namespace app\custom;
use Morrow\Debug;
use Morrow\Factory;

/**
* SETUP:
* 	To use permissions, you need to pass the permissions array, the default role and the role name delimiter to the constructor.
*	The permissions array must be like the following structure:
*
*		$myPermissions = array(
*			'my_permissions_type_1' => array(
*				'permissionName-1-1' => array('role_1, role_N'),
*				'permissionName-1-N' => array('role_1, role_N'),
*			),
*			'my_permissions_type_N' => array(
*				'permissionName-N-1' => array('role_1, role_N'),
*				'permissionName-N-N' => array('role_1, role_N'),
*			),
*		);
*
*	In words, you have to create a 3-dimensional array with the following entries:
*		1.dimension: permission types (just for grouping issues)
*		2.dimension: permission names
*		3.dimension: array of roles that are granted this permission
*
*	You can pass a mapping array as optional parameter. Permissions use this array to map the role identifiers.
* 	This feature aims to increase the readability of your permissions array:
*
*		$myMappingArray = array(
*			0 => 'role_1',
*			N => 'role_N',
*		);
*
*		$myPermissions = array(
*			'my_permissions_type_1' => array(
*				'permissionName-1-1' => array(0, N),
*				'permissionName-1-N' => array(0, N),
*			),
*			'my_permissions_type_N' => array(
*				'permissionName-N-1' => array(0, N),
*				'permissionName-N-N' => array(0, N),
*			),
*		);
*
* USAGE:
* 	- permission grouping:
*		You may group multiple permission names by using extended names. More specific names overwrite less specific ones.
*		You may optionally specify the permission delimiter in the constructor to enable this feature.
*
*		For example:
*		- grant permission to all actions that start with 'mail':
*			$myPermissions = array(
*				'actions' => array(
*					'mail' => array('admin', 'member'),
*				),
*			);
*		- overwrite this rule for one specific action, that also starts with 'mail':
*			$myPermissions = array(
*				'actions' => array(
*					'mail_special' => array('admin'),
*				),
*			);
*
*		- 'admin' and 'member' will be granted all permissions following the pattern '~mail_.*~' except for the specific 'mail_special' permission.
*/
class Permissions{
	private $_defaultRole         = null;
	private $_permissionDelimiter = null;
	private $_permissions         = null;
	private $_userRoles           = array();

	/**
	 * Constructor
	 *
	 * @param 	array 	$permissions 			the permissions array
	 * @param 	string 	$defaultRole 			the default user role
	 * @param 	string 	$permissionDelimiter 	the delimiter to seperate hierarchical permission names
	 * @param 	array 	$mappingArray 			the array containing the mapping information for better readability
	 */
	function __construct($permissions, $defaultRole, $permissionDelimiter = false, $mappingArray = false){
		$this->_permissions = $permissions;
		$this->_defaultRole = $defaultRole;
		$this->_permissionDelimiter = $permissionDelimiter;
		$this->_userRoles = array($defaultRole);

		// if a mappgin array is given, unmap all roles
		if($mappingArray){
			// unmap each role identifier
			foreach($this->_permissions as $groupName => $permissionGroup){
				foreach($permissionGroup as $permissionName => $roles){
					foreach($roles as $key => $role){
						$this->_permissions[$groupName][$permissionName][$key] = $mappingArray[$role];
					}
				}
			}
		}
	}

	/**
	 * Get current user roles
	 *
	 * @return array current user roles
	 */
	public function getUserRoles(){
		return $this->_userRoles;
	}

	/**
	 * Checks if any role has been granted
	 *
	 * @return bool default role is active
	 */
	public function anyRoleGranted(){
		if($this->_userRoles === array($this->_defaultRole)){
			return false;
		}
		return true;
	}

	/**
	 * Adds a role to the current instance
	 *
	 * @param 	string 	$roleToAdd 	the role to add to the current collection of granted roles
	 * @return void
	 */
	public function grantRole($roleToAdd){
		$this->_userRoles[] = $roleToAdd;
	}

	/**
	 * Checks if any of the granted roles has permission
	 *
	 * @param 	string 	$permissionGroup 	the role to add to the current collection of granted roles
	 * @param 	string 	$permissionName 	the permission to check for
	 * @param	mixed	$roles				roles to check permissions for (default is set up user roles)
	 * @return 	boolean 					permission granted
	 */
	public function hasPermission($permissionGroup, $permissionName, $roles = null){
		// set roles default value
		if(is_null($roles)){
			$roles = $this->_userRoles;
		}

		// accept non array value
		if(!is_array($roles)){
			$roles = array($roles);
		}

		// extract all possible permission names
		$permissionNames = array_keys($this->_permissions[$permissionGroup]);
		// find most fitting permission name
		$closestpermissionName = $this->_getClosestName($permissionName, $permissionNames);
		// if a permission name was found
		if($closestpermissionName){
			// get roles array
			$permittedRoles = $this->_permissions[$permissionGroup][$closestpermissionName];
			// if any of the user roles is one of the permitted roles
			if(array_intersect($roles, $permittedRoles)){
				// grant permission
				return true;
			}
		}
		// deny permission
		return false;
	}

	/**
	 * Get all roles that have a certain permission
	 *
	 * @param 	string 	$permissionGroup 	the role to add to the current collection of granted roles
	 * @param 	string 	$permissionName 	the permission to check for
	 * @return 	array 						roles that are granted a certain permission
	 */
	public function getRolesByPermission($permissionGroup, $permissionName){
		// extract all possible permission names
		$permissionNames = array_keys($this->_permissions[$permissionGroup]);
		// find most fitting permission name
		$closestpermissionName = $this->_getClosestName($permissionName, $permissionNames);
		// if a permission name was found
		if($closestpermissionName){
			// return roles array
			return $this->_permissions[$permissionGroup][$closestpermissionName];
		}
		// no roles found
		return false;
	}

	/**
	 * Search all permissions to find the closest permission name
	 *
	 * @param 	string $needle 		given permission name
	 * @param 	string $haystack 	given permission group
	 * @return	string 				resulting permission name
	 */
	private function _getClosestName($needle, $haystack){
		/**
		 * if there is a full match or no permissionDelimiter has been defined, return just the needle
		 */
		if(
			in_array($needle, $haystack)
			||
			$this->_permissionDelimiter === false
		){
			return $needle;
		}

		// explode the needle by the defined delimiter
		$needleShards = explode($this->_permissionDelimiter, $needle);

		// in case that there is not a full match, try to find the closest one
		$score = 0;
		$winner = false;
		foreach($haystack as $permissionName){
			// explode possible needle by defined delimiter
			$shards = explode($this->_permissionDelimiter, $permissionName);

			$scoreTemp = 0;
			// calculate score for this needle
			// compare substrings step by step
			foreach($shards as $key => $shard){
				if(isset($needleShards[$key])){
					if($shard == $needleShards[$key]){
						$scoreTemp++;
					}else{
						$scoreTemp = 0;
						break;
					}
				}else{
					// if possible result is more specific than the needle, disqualify it
					$scoreTemp = 0;
					break;
				}
			}
			// store highest score
			if($scoreTemp > $score){
				$score = $scoreTemp;
				$winner = $permissionName;
			}
		}
		return $winner;
	}
}
