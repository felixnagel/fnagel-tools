<?php

namespace app\modules\_main\lib;
use Morrow\Debug;

abstract class AbstractSlavery{
	protected $_slaves = [];
	protected $_master;

	public function control(&$slave){
		$this->_checkSubclass($slave);
		$this->_checkUniqueSlave($slave);
		$this->_addSlave($slave);
		$slave->_setMaster($this);
	}

	public function obey(&$master){
		$this->_setMaster($master);
		$master->_addSlave($this);
	}

	protected function _addSlave(&$slave){
		$this->_checkSubclass($slave);
		$this->_checkUniqueSlave($slave);
		$this->_slaves[] = $slave;
	}

	protected function _setMaster(&$master){
		$this->_checkSubclass($master);
		$this->_master = $master;
	}

	public function getSlaves(){
		return $this->_slaves;
	}

	protected function _checkSubclass($slave){
		if(!is_subclass_of($slave, __CLASS__)){
			throw new \Exception('Given object has to be derived from "' . __CLASS__ . '"!');
		}
	}
	protected function _checkUniqueSlave($slave){
		if(in_array($slave, $this->_slaves, true)){
			throw new \Exception('Given object is already managed by this object!');
		}
	}
}
