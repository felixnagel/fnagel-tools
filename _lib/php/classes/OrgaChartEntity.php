<?php

namespace app\modules\_main\lib;
use Morrow\Debug;

class OrgaChartEntity extends AbstractSlavery{
	private $_shape_class_prefix = '';

	public $horizontal_index;
	public $vertical_index;
	public $width;
	public $rel_width;
	public $shape;
	public $horizontal_offset;
	public $rel_horizontal_offset;

	/**
	 * Calculates and sets all public members.
	 * Execute this method before working with public members.
	 */
	public function prepare(){
		$this->width                 = $this->_calcWidth();
		$this->rel_width             = $this->_calcRelWidth();

		$this->horizontal_index      = $this->_calcHorizontalIndex();
		$this->vertical_index        = $this->_calcVerticalIndex();

		$this->horizontal_offset     = $this->_calcHorizontalOffset();
		$this->rel_horizontal_offset = $this->_calcRelHorizontalOffset();

		$this->shape                 = $this->_calcShape();
	}

	/**
	 * Returns the team leader.
	 *
	 * @return object Team Leader
	 */
	private function _getLeader(){
		if(is_null($this->_master)){
			return $this;
		}
		return $this->_master->_getLeader();
	}

	/**
	 * Calculates the width of a member.
	 * Each member's width is the sum of its slave members' widths.
	 * A single member has a width of 1.
	 *
	 * @return int Width of member
	 */
	private function _calcWidth(){
		if(!$this->_slaves){
			return 1;
		}
		$width = 0;
		foreach($this->_slaves as $slave){
			$width += $slave->_calcWidth();
		}
		return $width;
	}

	private function _calcRelWidth(){
		return $this->_calcWidth() / $this->_getLeader()->_calcWidth();
	}

	private function _calcVerticalIndex($verticalPosition = 0){
		if(is_null($this->_master)){
			return $verticalPosition;
		}
		return $this->_master->_calcVerticalIndex(++$verticalPosition);
	}

	private function _calcHorizontalIndex(){
		return array_search($this, $this->_getLeader()->_collectGlobalSiblings($this->_calcVerticalIndex()), true);
	}

	private function _calcShape(){
		$siblings = $this->_getSiblings();
		$shape = [];
		if((bool)$this->_master){
			$shape[] = $this->_shape_class_prefix . 'has_master';
		}
		if((bool)count($this->_slaves)){
			$shape[] = $this->_shape_class_prefix . 'has_slave';
		}
		if((end($siblings) !== $this)){
			$shape[] = $this->_shape_class_prefix . 'has_right_sibling';
		}
		if((reset($siblings) !== $this)){
			$shape[] = $this->_shape_class_prefix . 'has_left_sibling';
		}
		$shape = implode($shape, ' ');
		return $shape;
	}

	private function _collectGlobalSiblings($depth, &$globalSiblings = []){
		if($this->_calcVerticalIndex() === $depth){
			$globalSiblings[] = $this;
		}else{
			foreach($this->_slaves as $slave){
				$slave->_collectGlobalSiblings($depth, $globalSiblings);
			}
		}
		return $globalSiblings;
	}

	private function _getSiblings(){
		if(is_null($this->_master)){
			return [$this];
		}
		return $this->_master->getSlaves();
	}

	private function _calcHorizontalOffset(){
		if(is_null($this->_master)){
			return 0;
		}
		$offset = $this->_master->_calcHorizontalOffset();

		foreach($this->_getSiblings() as $key => $slave){
			if($slave === $this){
				break;
			}

			$offset += $slave->_calcWidth();
		}

		return $offset;
	}

	private function _calcRelHorizontalOffset(){
		return $this->_calcHorizontalOffset() / $this->_getLeader()->_calcWidth();
	}
}
