<?php

namespace app\custom;
use Morrow\Debug;
use Morrow\Factory;
use \GetId3\GetId3Core as GetId3;

class Help{
	/**
	 * Return current formatted time
	 *
	 * @param	string	$format	The format string
	 * @return 	string			The formatted time
	 */
	public static function now($format = null){
		if($format === null){
			$format = 'Y-m-d H:i:s';
		}
		return Factory::load('\datetime')->format($format);
	}

	/**
	 * Create a source url for any favicon via google api
	 *
	 * @param 	string	$url	The url of any website
	 * @return 	string			The generated url delivered by google api
	 */
	public static function getFavIcon($url){
		return Factory::load('Url')->create('http://www.google.com/s2/favicons', array('domain' => $url));
	}

	/**
	 * Generate a fluent color encoding
	 *
	 * Example:
	 *	$val = 3.1415;
     *  $colorEncodings = array(
     *  	'4' => array('r' =>   0, 'g' => 255, 'b' =>   0),
     *      '3' => array('r' => 255, 'g' => 255, 'b' =>   0),
     *      '2' => array('r' => 255, 'g' => 128, 'b' =>   0),
     *      '1' => array('r' => 128, 'g' =>   0, 'b' =>   0)
     *  );
	 *
	 * @param 	int		$val			The value to generate the color encoding for
	 * @param  	array	$colorEncodings The color encodings array
	 * @return 	string					The generated hex color value
	 */
	public static function getColorEncoding($val, $colorEncodings){
	    ksort($colorEncodings);

		$valMin = key($colorEncodings);
	    end($colorEncodings);
	    $valMax = key($colorEncodings);

	    foreach($colorEncodings as $threshold => $color){
	        if($threshold <= $val){
	            $valMin = $threshold;
	        }
	        if($threshold >= $val){
	            $valMax = $threshold;
	            break;
	        }
	    }

	    // get color encoding of enclosing scores
	    $colorMax = $colorEncodings[(string)$valMax];
	    $colorMin = $colorEncodings[(string)$valMin];

	    $result = '#';

	    foreach(array('r', 'g', 'b') as $key){
	        // if exact color
	        if($valMin == $valMax){
	            $color = $colorMax[$key];
	        // if value is in between two colors
	        }else{
	            // get color encoding between two scores
	            $color = round($colorMin[$key] + ($val - $valMin) / ($valMax - $valMin) * ($colorMax[$key] - $colorMin[$key]));
	        }

	        // convert to hex
	        $color = dechex($color);
	        if(strlen($color) < 2){
	            $color = '0' . $color;
	        }
	        $result = $result . $color;
	    }
	    return $result;
	}

	/**
	 * Format time string from 'd.m.y' to 'Y-m-d H:i:s'
	 *
	 * @param	string	$string		The time string to format
	 * @return 	string				The formatted time string
	 */
	public static function dmyToYmdHis($string){
		$time = \DateTime::createFromFormat('d.m.y H:i:s', $string . ' 00:00:00');
		return $time->format('Y-m-d H:i:s');
	}

	/**
	 * Format time string from 'Y-m-d H:i:s' to 'd.m.Y'
	 *
	 * @param	string	$string		The time string to format
	 * @return 	string				The formatted time string
	 */
	public static function ymdToDmY($string){
		$time = \DateTime::createFromFormat('Y-m-d H:i:s', $string);
		return $time->format('d.m.Y');
	}

	/**
	 * Format time string from 'Y-m-d H:i:s' to 'd.m.Y H:i:s'
	 *
	 * @param	string	$string		The time string to format
	 * @return 	string				The formatted time string
	 */
	public static function ymdToDmYHis($string){
		$time = \DateTime::createFromFormat('Y-m-d H:i:s', $string);
		return $time->format('d.m.Y H:i:s');
	}

	/**
	 * Format time string from 'd.m.Y' to 'd.m.y'
	 *
	 * @param	string	$string		The time string to format
	 * @return 	string				The formatted time string
	 */
	public static function dmYToDmy($string){
		$time = \DateTime::createFromFormat('d.m.Y', $string);
		return $time->format('d.m.y');
	}


	/**
	 * Calculates the number of days from now to given time string
	 *
	 * @param	string	$ymd	The time string to calculate the difference of days from, formatted as 'Y-m-d'
	 * @return 	int				The calculated difference of days
	 */
	public static function diffNowToYmdDays($ymd){
		$date = new \DateTime($ymd);
		$date->setTime(0, 0, 0);
		$now = new \DateTime();
		$now->setTime(0, 0, 0);
		$diff = $now->diff($date);
		return $diff->invert ? -$diff->days : $diff->days;
	}

	/**
	 * Create a time string with a given time modifier, formatted as 'Y-m-d H:i:s'
	 *
	 * @param	string	$modifier	The given date modifier ('+ 1 days', '- 10 years' etc.)
	 * @return 	string				The generated time string
	 */
	public static function futureDateTime($modifier){
		$date = new \DateTime();
		$date->modify($modifier);
		return $date->format('Y-m-d H:i:s');
	}

	/**
	 * Remove any directory recursively
	 *
	 * Symlink will be ignored using is_link().
	 *
	 * @param  string  $folderPath The path of the folder to remove
	 * @param  boolean $retainEmptyFolder|false If this optional flag is set to true, the specified folder itself will not be removed and will remain empty
	 */
	public static function removeDirRecursive($folderPath, $retainEmptyFolder = false){
		if(is_dir($folderPath)){
			$objects = scandir($folderPath);
			foreach($objects as $object){
				if($object != '.' && $object != '..'){
					$path = $folderPath . '/' . $object;
					if(filetype($path) == 'dir'){
						self::removeDirRecursive($path);
					}else{
						if(is_link($path)){
							Factory::load('Log')->set(__FUNCTION__ . ': SYMLINK IGNORED - ' . $path);
							continue;
						}
						@unlink($path);
					}
				}
			}
			reset($objects);
			if(!$retainEmptyFolder){
				@rmdir($folderPath);
			}
		}
	}

	/**
	 * Stores a file that has been submitted in a form and returns its url.
	 * If destination folder does not exist, it will be created recursively.
	 *
	 * @param  	array 	$file 			The uploaded file data array
	 * @param  	string 	$folderRelPath	The path to the destination folder, relative to the framework's PUBLIC_PATH
	 * @return 	string 					Url of stored file
	 */
	public static function storePublicFile($file, $folderRelPath){
		// create absolute folder path and create folder
		$folderPath = PUBLIC_PATH . $folderRelPath;
		if(!is_dir($folderPath)){
			mkdir($folderPath, 0755, true);
		}

		// create file url and path
		$url = $folderRelPath . $file['name'];
		$path = PUBLIC_PATH . $url;

		// move file to destination folder
		move_uploaded_file($file['tmp_name'], $path);

		return $url;
	}

	/**
	 * Prepare select box choices for the form handler.
	 *
	 * Puts all defined choices with their translations into an array. Choice definitions must be set up in config.
	 * The first entry will be 'Please choose' by default but may be disabled with the optional $defaultNull flag.
	 *
	 * Template variables:
	 * 	- array	${$configIdentifier}	the select choices array
	 *
	 * @param  string  $configIdentifier The config key of the defined select choices
	 * @param  boolean $defaultNull|true The $defaultNull flag
	 * @return array The select box choices array
	 */
	public static function prepareFixedSelectChoices($configIdentifier, $defaultNull = true){
		$selectChoices = Factory::load('Config')->get($configIdentifier);

		/**
		 * if the default value should be null (default)
		 * and it is not defined in the config
		 */
		if($defaultNull && !isset($selectChoices[null])){
			$selectChoices[null] = 'Please choose';
		}

		/**
		 * run language function on every displayed text
		 */
		foreach($selectChoices as $value => $displayText){
			$selectChoices[$value] = Factory::load('Language')->_($displayText);
		}

		/**
		 * set to template
		 */
		return $selectChoices;
	}

	/**
	 * uses form data to store a node icon
	 *
	 * @param	array	$formData	The form data array, has to include the fields 'website_url' and 'node_icon_url'
	 * @param  	string	$filename	Filename
	 * @return 	string				url of stored node icon
	 */
	public static function storeNodeIconFromFormData($formData, $filename){
		/**
		 * get destination directory and create it if necessary
		 */
		$url = Factory::load('Config')->get('node-icon-base-url');
		$destinationDir = PUBLIC_PATH . $url;
		if(!is_dir($destinationDir)){
			mkdir($destinationDir, 0755, true);
		}
		// create file path
		$filePath = $destinationDir . $filename;

		/**
		 * store file
		 * NOTE: both fields are not required
		 * NOTE: if both fields are filled, the lesser one will be used
		 */
		// if user submitted a website url, copy its favicon
		if(strlen($formData['website_url'])){
			$source = 'http://www.google.com/s2/favicons?domain=' . rtrim($formData['website_url'], '/');
			$result = copy($source, $filePath);
		}
		// if user submitted a file, copy that one
		if($formData['node_icon_url_file']['error'] !== 4){
			move_uploaded_file($formData['node_icon_url_file']['tmp_name'], $filePath);
		}

		return $url . $filename;
	}

	/**
	 * Searches a folder recursively for a file. Ignores symlinks. Returns false when nothing has been found.
	 * Only return the path of the file it found first.
	 * @param	string	$folder	The folder to search in
	 * @param  	string 	$file	The filename to search for
	 * @return 	mixed  			The path to found file or false if no file has been found.
	 */
	public static function findFileRecursive($folder, $file){
		$folder = rtrim($folder, '/');

		// scan this folder
		$scan = scandir($folder);

		foreach($scan as $key => $node){
			// ignore '.' and '..' entries
			if($node === '.' || $node === '..'){
				unset($scan[$key]);
				continue;
			}

			// ignore symlinks
			if(is_link($folder . '/' . $node)){
				Factory::load('Log')->set(__FUNCTION__ . ': SYMLINK IGNORED - ' . $folder . '/' . $node);
				continue;
			}

			// if file is found, return its path
			if($node === $file){
				return $folder . '/' . $file;
			}
		}

		// is the file has not been found at this level, search in next folder
		foreach($scan as $key => $node){
			$subfolder = $folder . '/' . $node;
			// call self on next folder
			if(is_dir($subfolder)){
				return self::findFileRecursive($subfolder, $file);
			}
		}

		return false;
	}
}
