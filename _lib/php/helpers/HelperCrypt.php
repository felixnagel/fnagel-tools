<?php

namespace app\custom;
use Morrow\Debug;
use Morrow\Factory;

class HelperCrypt{
	private $_characterStock          = null;
	private $_hashCost                = null;
	private $_passwordMinLength       = null;
	private $_verificationTokenLength = null;

	function __construct(){
		$this->_characterStock          = Factory::load('Config')->get('password_character-stock');
		$this->_hashCost                = Factory::load('Config')->get('password_hash-cost');
		$this->_passwordMinLength       = Factory::load('Config')->get('password_min-length');
		$this->_verificationTokenLength = Factory::load('Config')->get('token_length');
	}

	/**
	 * Create a hash from any string
	 *
	 * @param	string	$string		The string to create the hash from
	 * @return 	string				The generated hash
	 */
	public function createHash($string){
		return password_hash($string, PASSWORD_BCRYPT, array('cost' => $this->_hashCost));
	}

	/**
	 * Create a random password
	 *
	 * @return 	string	The genertaed password
	 */
	public function createPassword(){
		return $this->generateRandomString($this->_passwordMinLength);
	}

	/**
	 * Create a random verification token
	 *
	 * @return	string	The generated verification token
	 */
	public function createVerificationToken(){
		return $this->generateRandomString($this->_verificationTokenLength);
	}

	/**
	 * Compare a password with its hash
	 *
	 * @param	string	$password	The password
	 * @param  	string	$hash		The hash
	 * @return 	bool				Password is valid
	 */
	public function verifyPassword($password, $hash){
		return password_verify($password, $hash);
	}

	/**
	 * Create a random string
	 *
	 * @param  	int		$length		The length of the string
	 * @return 	string				The generated string
	 */
	public function generateRandomString($length){
		$randomString = '';
		$i = 0;

		while($i++ < $length){
			$randomString .= substr($this->_characterStock, mt_rand(0, (strlen($this->_characterStock) - 1)), 1);
		}

		return $randomString;
	}
}
