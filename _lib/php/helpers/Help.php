<?php

namespace app\modules\_main\lib;
use Morrow\Debug;
use Morrow\Factory;


class Help{
	/**
	 * Return current formatted time
	 *
	 * @param	string	$format	The format string
	 * @return 	string			The formatted time
	 */
	public static function now($format = null){
		if($format === null){
			$format = 'Y-m-d H:i:s';
		}
		return Factory::load('\datetime')->format($format);
	}

	/**
	 * Remove any directory recursively
	 *
	 * Symlink will be ignored using is_link().
	 *
	 * @param  string  $folderPath The path of the folder to remove
	 * @param  boolean $retainEmptyFolder|false If this optional flag is set to true, the specified folder itself will not be removed and will remain empty
	 */
	public static function removeDirRecursive($folderPath, $retainEmptyFolder = false){
		if(is_dir($folderPath)){
			$objects = scandir($folderPath);
			foreach($objects as $object){
				if($object != '.' && $object != '..'){
					$path = $folderPath . '/' . $object;
					if(filetype($path) == 'dir'){
						self::removeDirRecursive($path);
					}else{
						if(is_link($path)){
							Factory::load('Log')->set(__FUNCTION__ . ': SYMLINK IGNORED - ' . $path);
							continue;
						}
						@unlink($path);
					}
				}
			}
			reset($objects);
			if(!$retainEmptyFolder){
				@rmdir($folderPath);
			}
		}
	}

	/**
	 * Stores a file that has been submitted in a form and returns its url.
	 * If destination folder does not exist, it will be created recursively.
	 *
	 * @param  	array 	$file 			The uploaded file data array
	 * @param  	string 	$folderPath		The path to the destination folder
	 * @param	bool	$autoFilename 	Flag that indicates that the filename should be generated automatically (current time 'Y-m-d_H-i-s').
	 * @return 	string 					filename
	 */
	public static function storePublicImage($file, $folderPath, $autoFilename = null){
		Debug::dump($folderPath);
		die();


		// create folder
		if(!is_dir($folderPath)){
			mkdir($folderPath, 0755, true);
		}

		if($autoFilename){
			// get file extension
			$extension = (new \SplFileInfo($file['name']))->getExtension();
			$filename = self::now('Y-m-d_H-i-s') . '.' . $extension;
		}else{
			$filename = $file['name'];
		}

		$path = $folderPath . $filename;

		// move file to destination folder
		move_uploaded_file($file['tmp_name'], $path);

		return $filename;
	}

	/**
	 * Searches a folder recursively for a file. Ignores symlinks. Returns false when nothing has been found.
	 * Only return the path of the file it found first.
	 * @param	string	$folder	The folder to search in
	 * @param  	string 	$file	The filename to search for
	 * @return 	mixed  			The path to found file or false if no file has been found.
	 */
	public static function findFileRecursive($folder, $file){
		// scan this folder
		$scan = scandir($folder);

		foreach($scan as $key => $node){
			// ignore '.' and '..' entries
			if($node === '.' || $node === '..'){
				unset($scan[$key]);
				continue;
			}

			// ignore symlinks
			if(is_link($folder . '/' . $node)){
				Factory::load('Log')->set(__FUNCTION__ . ': SYMLINK IGNORED - ' . $folder . '/' . $node);
				continue;
			}

			// if file is found, return its path
			if($node === $file){
				return $folder . '/' . $file;
			}
		}

		// is the file has not been found at this level, search in next folder
		foreach($scan as $key => $node){
			$subfolder = $folder . '/' . $node;
			// call self on next folder
			if(is_dir($subfolder)){
				return self::findFileRecursive($subfolder, $file);
			}
		}

		return false;
	}
}
