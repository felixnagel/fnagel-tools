<?php

namespace app\modules\_main\models;

use Morrow\Factory;
use Morrow\Debug;
use app\modules\_main\lib\Help;

class SimpleLanguageExtension extends AlphaModel{
	protected $_lang;

	public function __construct(){
		$this->_lang = Factory::load('Language')->get();
		$this->_possibleLanguages = Factory::load('Language')->getPossible();
		parent::__construct();
	}

	public function get($conditions = null, $isTranslated = true, $orderBy = null, $single = null){
		$result = parent::get($conditions, $orderBy, $single);

		if($result){
			if($single){
				$result = [$result];
			}

			if($isTranslated){
				$result = $this->_translateResult($result);
				$result = $this->_removeTranslationsFromResult($result);
			}

			if($single){
				$result = array_pop($result);
			}
		}
		return $result;
	}

	public function getSingle($conditions, $isTranslated = true){
		return $this->get($conditions, $isTranslated, null, true);
	}

	private function _translateResult($result){
		$pattern = "=^(.+?)_lang_" . $this->_lang . "$=";
		foreach($result as $key => $row){
			foreach($row as $field => $value){
				preg_match_all($pattern, $field, $matches);
				if($matches[0] && $matches[1]){
					$result[$key][$matches[1][0]] = $result[$key][$matches[0][0]];
				}
			}
		}
		return $result;
	}

	private function _removeTranslationsFromResult($result){
		foreach($this->_possibleLanguages as $key => $possibleLanguage){
			if($key === 0){
				continue;
			}
			$pattern = "=^(.+?)_lang_" . $possibleLanguage . "$=";
			foreach($result as $key => $row){
				foreach($row as $field => $value){
					preg_match_all($pattern, $field, $matches);
					if($matches[0] && $matches[1]){
						unset($result[$key][$matches[0][0]]);
					}
				}
			}
		}
		return $result;
	}
}
