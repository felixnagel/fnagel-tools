<?php

namespace app\modules\_main\models;

use Morrow\Factory;
use Morrow\Debug;
use app\modules\_main\lib\Help;

class AlphaModel extends \Morrow\AbstractTableDataGateway{
	/**
	 * Database to interact with
	 * @var null
	 */
	protected $_db = null;

	/**
	 * Default name of table
	 * @var null
	 */
	protected $_table = null;

	/**
	 * Fields that are allowed to be inserted
	 * @var array
	 */
	protected $_allowed_insert_fields = array();

	/**
	 * Fields that are allowed to be inserted by default
	 * @var array
	 */
	protected $_allowed_insert_fields_default = array(
		'created_at',
		'created_by',
	);

	/**
	 * Fields that are allowed to be updated
	 * @var array
	 */
	protected $_allowed_update_fields = array();

	/**
	 * Fields that are allowed to be updated by default
	 * @var array
	 */
	protected $_allowed_update_fields_default = array(
		'updated_at',
		'updated_by',
	);

	/**
	 * [$_fileFields description]
	 * @var [type]
	 */
	protected $_fileFields = [];

	/**
	 * Id of the current user
	 * @var	null
	 */
	protected $_userId = null;

	/**
	 * Default model constructor
	 *
	 * Merges default allowed insert and update fields with the specified ones
	 * and sets the current user id.
	 *
	 * @param	int	 $userId 	The id of the current user
	 */
	public function __construct($userId = null){
		$this->_db = Factory::load('Db');
		$this->_allowed_insert_fields = array_merge($this->_allowed_insert_fields_default, $this->_allowed_insert_fields);
		$this->_allowed_update_fields = array_merge($this->_allowed_update_fields_default, $this->_allowed_update_fields);

		if(is_numeric($userId)){
			$this->_userId = $userId;
		}
	}

	/**
	 * override insert method
	 *
	 * Automatically set the field 'created_by' and execute the parent insert method
	 *
	 * @param 	array  	$data	The insert data array
	 * @return	array 			The insert query result array
	 */
	public function insert(array $data){
		if(!isset($data['created_by'])){
			$data['created_by'] = $this->_userId;
		}
		$this->_extractFileFields($data);
		return parent::insert($data);
	}

	/**
	 * override update method
	 *
	 * Automatically set the field 'updated_by' and execute the parent update method
	 *
	 * @param 	array  	$data       	The update data array
	 * @param  	mixed	$conditions		The conditions argument to pass to update()
	 * @return	array 					The update query result array
	 */
	public function update($data, $conditions){
		if(!isset($data['updated_by'])){
			$data['updated_by'] = $this->_userId;
		}
		$this->_extractFileFields($data);
		return parent::update($data, $conditions);
	}

	public function updateFileFields($data, $id){
		if(!isset($data['updated_by'])){
			$data['updated_by'] = $this->_userId;
		}
		$uploadedFileFields = $this->_extractFileFields($data);

		foreach($uploadedFileFields as $field => $file){
			if($file['error'] != 4){
				$path = sprintf($this->_fileFields[$field], $id);
				$filename = $this->_storeFile($file, $path);
				$uploadedFileFields[$field] = $filename;
			}else{
				unset($uploadedFileFields[$field]);
			}
		}

		if($uploadedFileFields){
			if(!isset($data['updated_by'])){
				$data['updated_by'] = $this->_userId;
			}
			return parent::update($uploadedFileFields, $id);
		}
	}


	/**
	 * Gets all data from current tabel
	 *
	 * @param 	string  $orderBy			The name of the column to order by
	 * @param 	string 	$orderMode | 'ASC' 	The order mode flag ('ASC', 'DESC')
	 * @return	array 						The processed query result array
	 */
	public function getAll($orderBy = 'id', $orderMode = 'ASC'){
		$result = $this->_db->get("
			SELECT *
			FROM {$this->_table}
			ORDER BY {$orderBy} {$orderMode};"
		);
		return $this->_returnResult($result);
	}

	/**
	 * override get method
	 *
	 * Pass the default result of get() to _returnResult()
	 *
	 * @param  	mixed 	$conditions | null	The conditions argument to pass to get()
	 * @return 	array 						The processed query result array
	 */
	public function get($conditions = null, $orderBy = null, $single = null){
		return $this->_returnResult(parent::get($conditions, $orderBy), $single);
	}

	/**
	 * Default search function for all models (generic search query)
	 *
	 * This query will search for the term in following fields:
	 * 	- 'name'
	 *
	 * @param  string $term The search string
	 * @return array The processed query result array
	 */
	public function search($term){
		$token = array('%' . $term . '%');
		$result = $this->_db->get("
			SELECT *
			FROM {$this->_table}
			WHERE name LIKE ?
			ORDER BY name ASC;",
			$token
		);
		return $this->_returnResult($result);
	}

	/**
	 * Overloaded get method
	 *
	 * Pass the default result of get() to _returnResult() with the $returnAsSingleRowResult flag set to true
	 *
	 * @param 	array	$conditions | null	The conditions array to pass to get()
	 * @return	array 						The processed query result array
	 */
	public function getSingle($conditions){
		return $this->get($conditions, null, true);
	}

	/**
	 * Prepares the output of get query result array
	 *
	 * Specific data fields will be modified:
	 *  - 'specific_field': description of change
	 *  - 'archived_at', 'delivery_date', 'start_of_ad': The date format will be changed to dmY.
	 *
	 * Array keys will be set according to the data row ids.
	 * Results may be set up as single row results.
	 *
	 * @param 	array 	$result 							The Query result array to be processed
	 * @param 	boolean $returnAsSingleRowResult | false	Set this flag to true to return a single row result array
	 * @return 	array 										The processed query result array
	 */
	protected function _returnResult($result, $returnAsSingleRowResult = false){
		if(isset($result['RESULT'])){
			$result = $result['RESULT'];
		}

		/**
		 * By default, put ids as keys of result array
		 */
		$resultTemp = array();
		foreach($result as $resultPart){
			$resultTemp[$resultPart['id']] = $resultPart;
		}
		$result = $resultTemp;

		/**
		 * Change file fields to represent the full paths
		 */
		foreach($result as $id => $row){
			foreach($this->_fileFields as $field => $path){
				if(isset($row[$field])){
					$result[$id][$field] = sprintf($path, $id) . $row[$field];
				}
			}
		}

		/**
		 * Optionally, force to output a single value instead of an array
		 */
		if($returnAsSingleRowResult === true){
			$result = array_pop($result);
		}

		return $result;
	}

	protected function _storeFile($file, $folder){
		// delete folder content
		if(is_dir($folder)){
			Help::removeDirRecursive($folder, true);
		}

		// create folder
		if(!is_dir($folder)){
			mkdir($folder, 0755, true);
		}

		// get file extension
		$extension = (new \SplFileInfo($file['name']))->getExtension();
		$filename = date('Y-m-d_H-i-s') . '.' . $extension;

		$path = $folder . $filename;

		// move file to destination folder
		move_uploaded_file($file['tmp_name'], $path);

		return $filename;
	}

	protected function _extractFileFields(&$data){
		$uploadedFileFields = array_intersect_key($data, $this->_fileFields);
		$data = array_diff_key($data, $this->_fileFields);

		return $uploadedFileFields;
	}
}
