function ProjectionPlane(options){
	this.p        = options.cPosition;
	this.theta    = options.cameraInclination /180*Math.PI;
	this.phi      = options.cameraAzimuth /180*Math.PI;


	this.setViewport(options.pwWidth, options.pwHeight);
	this.setViewingAngle(options.alpha);


	console.log(this.ar);

};

ProjectionPlane.prototype = {
	_r1: [],
	_r2: [],
	_r3: [],
	_r4: [],
	_rFinal: [],

	project: function project(r){
		// translate to origin
		this._r1[0] = r[0] - this.p[0];
		this._r1[1] = r[1] - this.p[1];
		this._r1[2] = r[2] - this.p[2];

		// rotate at z axis
		this._r2[0] = this._r1[0]*Math.cos(-this.phi)	+ this._r1[1]*Math.sin(-this.phi) 	+ 0;
		this._r2[1] = this._r1[0]*-Math.sin(-this.phi)	+ this._r1[1]*Math.cos(-this.phi)	+ 0;
		this._r2[2] = 0									+ 0									+ this._r1[2];

		// rotate at y axis
		this._r3[0] = this._r2[0]*Math.cos(-this.theta)	+ 0 			+ this._r2[2]*-Math.sin(-this.theta);
		this._r3[1] = 0									+ this._r2[1]	+ 0;
		this._r3[2] = this._r2[0]*Math.sin(-this.theta) + 0				+ this._r2[2]*Math.cos(-this.theta);

		// add d value to z axis
		this._r4[0] = this._r3[0];
		this._r4[1] = this._r3[1];
		this._r4[2] = this._r3[2] + this.d;

		// normalize
		this._r4[0] = this._r4[0] / this.ar;
		this._r4[1] = this._r4[1];
		this._r4[2] = this._r4[2];

		// projection
		this._rFinal[0] = this._r4[0] * this.d / this._r4[2];
		this._rFinal[1] = this._r4[1] * this.d / this._r4[2];

		// denormalize
		this._rFinal[0] = this._rFinal[0] * this.ar + 0.5*this.pwWidth;
		this._rFinal[1] = this._rFinal[1] + 0.5*this.pwHeight;

		return this._rFinal;
	},

	setViewingAngle: function setV(angle){
		this.cAngle = angle /180*Math.PI;
		this.d       = 0.5*this.pwHeight / Math.tan(0.5*this.cAngle);
	},

	setViewport: function setViewport(width, height){
		this.pwWidth = width;
		this.pwHeight = height;
		this.ar = this.pwWidth / this.pwHeight;
	},

	setCamera: function setCamera(properties){
		if(typeof properties.x !== 'undfined'){
			this.p[0] = properties.x;
		}
		if(typeof properties.y !== 'undfined'){
			this.p[1] = properties.y;
		}
		if(typeof properties.z !== 'undfined'){
			this.p[3] = properties.z;
		}
		if(typeof properties.inclination !== 'undfined'){
			this.theta = properties.inclination /180*Math.PI;
		}
		if(typeof properties.azimuth !== 'undfined'){
			this.phi = properties.azimuth /180*Math.PI;
		}
	}
};
