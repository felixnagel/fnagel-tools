<!doctype html>
<html>
<head>
	<style>
	#canvas{margin:0 auto; display:block; border:1px solid #000000;}
	#left-div{width:45%; display:inline-block;}
	#right-div{width:45%; display:inline-block;}
	#camera-form input{display:block; width:100%;}

	</style>
</head>
<body>

	<div id="left-div">
		<canvas id="canvas" width="600" height="400"></canvas>

	</div>

	<div id="right-div">
		<form id="camera-form">
			<div>
				<label>x:<span id="label-camera-x"></span></label><input id="camera-x" type="range" min="-500" max="500" step="1" value="0" />
				<label>y:<span id="label-camera-y"></span></label><input id="camera-y" type="range" min="-500" max="500" step="1" value="0" />
				<label>z:<span id="label-camera-z"></span></label><input id="camera-z" type="range" min="-200" max="200" step="1" value="0" />
			</div>

			<br />

			<div>
				<label>theta:<span id="label-camera-theta"></span></label><input id="camera-theta" type="range" min="-90" max="90" step="0" value="0" />
				<label>phi:<span id="label-camera-phi"></span></label><input id="camera-phi" type="range" min="-180" max="180" step="0" value="0" />
			</div>
		</form>
	</div>

</body>

<script src="../js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="ProjectionPlane.js" type="text/javascript"></script>

<script type="text/javascript">
$(function(){

	var
		$canvas = $('#canvas'),
		ctx     = $canvas[0].getContext('2d');

	var
		pwWidth  = $canvas.width(),
		pwHeight = $canvas.height();

	var
		polygons = {
			_1: [
				[ 200,  200, 200],
				[-200,  200, 200],
				[-200, -200, 200],
				[ 200, -200, 200],
			]
		};

	ctx.fillStyle   = '#FFAAAA';
	ctx.strokeStyle = '#000000';
	ctx.lineWidth   = 1;

	var
		projectionPlane = new ProjectionPlane({
			cPosition: [0, 0, 0],
			inclination: 0,
			azimuth: 0,
			pwWidth: pwWidth,
			pwHeight: pwHeight,
			alpha: 40,
		});

	var
		projectedPoint = [],
		methods = {
			drawPolygon: function drawPolygon(points){
				ctx.clearRect(0, 0, pwWidth, pwHeight);
				ctx.beginPath();

				projectedPoint = projectionPlane.project([
					points[points.length-1][0],
					points[points.length-1][1],
					points[points.length-1][2]
				]);

				ctx.moveTo(projectedPoint[0], projectedPoint[1]);

				for(var key in points){
					projectedPoint = projectionPlane.project([
						points[key][0],
						points[key][1],
						points[key][2]
					]);
					ctx.lineTo(
						projectedPoint[0],
						projectedPoint[1]
					);
				}

				ctx.closePath();
				ctx.fill();
				ctx.stroke();
			},
		};

	methods.drawPolygon(polygons._1);




	var
		$cphi   = $('#camera-phi'),
		$ctheta = $('#camera-theta'),
		$cx     = $('#camera-x'),
		$cy     = $('#camera-y'),
		$cz     = $('#camera-z');

	$cx.on('change mousemove', function(){
		$('#label-'+$(this).attr('id')).html($(this).val());
		projectionPlane.p[0] = parseInt($(this).val());
		methods.drawPolygon(polygons._1);
	});
	$cy.on('change mousemove', function(){
		$('#label-'+$(this).attr('id')).html($(this).val());
		projectionPlane.p[1] = parseInt($(this).val());
		methods.drawPolygon(polygons._1);
	});
	$cz.on('change mousemove', function(){
		$('#label-'+$(this).attr('id')).html($(this).val());
		projectionPlane.p[2] = parseInt($(this).val());
		methods.drawPolygon(polygons._1);
	});
	$ctheta.on('change mousemove', function(){
		$('#label-'+$(this).attr('id')).html($(this).val());
		projectionPlane.theta = parseInt($(this).val())/180*Math.PI;
		methods.drawPolygon(polygons._1);
	});
	$cphi.on('change mousemove', function(){
		$('#label-'+$(this).attr('id')).html($(this).val());
		projectionPlane.phi = parseInt($(this).val())/180*Math.PI;
		methods.drawPolygon(polygons._1);
	});
});




</script>



</html>
