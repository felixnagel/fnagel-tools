<?php
	$topA = 300;
	$leftA = 300;
	$widthA = 200;
	$heightA = 200;

	$widthB = 50;
	$heightB = sqrt(pow($widthA, 2) + pow($heightA, 2));
	$rotB = 160;

	$posB = positionR($rotB*M_PI/180, $widthA, $heightA);

	$topB = $topA - 0.5*($heightB - $heightA) + $posB['top'];
	$leftB = $leftA + 0.5*$widthA + $posB['left'];

	function positionR($phi, $w, $h){
		$phi = fmod($phi, 2*M_PI);

		if($phi >= 0.0*M_PI && $phi < 0.5*M_PI || $phi >= 1.0*M_PI && $phi < 1.5*M_PI){
			$gamma = atan($h/$w);
		}
		if($phi >= 0.5*M_PI && $phi < 1.0*M_PI || $phi >= 1.5*M_PI && $phi < 2.0*M_PI){
			$gamma = atan($w/$h);
		}

		$alpha = $gamma - fmod($phi, 0.5*M_PI);
		$r = sqrt(pow(0.5*$w, 2) + pow(0.5*$h, 2)) * cos($alpha);

		return [
			'top' => $r*sin($phi),
			'left' => $r*cos($phi),
		];
	}

?>

<!doctype html>
<html>
<head>
	<style>
		#a{position:absolute; top:<?php echo $topA;?>px; left:<?php echo $leftA;?>px; width:<?php echo $widthA;?>px; height:<?php echo $heightA;?>px; border:1px solid red;}
		#b{position:absolute; top:<?php echo $topB;?>px; left:<?php echo $leftB;?>px; width:<?php echo $widthB;?>px; height:<?php echo $heightB;?>px; border:1px solid red; transform:rotate(<?php echo $rotB;?>deg); transform-origin: 0 50%;}
	</style>
</head>
<body>
	<div id="a"></div>
	<div id="b"></div>
</body>
</html>
