<!doctype html>
<html>
<head>
	<link rel="stylesheet" href="knob.css" />
	<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="../js/fnagel-knob_jquery-plugin.js"></script>
</head>
<body>

	<div id="knob">
		<div class="rotating">
			<div class="pointer"></div>
			<div class="scratches"></div>
		</div>

		<div class="shadow3"></div>

		<div class="wriggling">
			<div class="shadow2"></div>
			<div class="shadow"></div>
		</div>

	</div>






</body>

<script type="text/javascript">
	'use-strict';

	$(document).ready(function(){
		var $knob = $('#knob');

		$knob.fnagelKnob({
			'$rotatingParts': $knob.find('.rotating'),
			'$wrigglingParts': $knob.find('.wriggling'),
			maxAngle: 300,
			minAngle: 60
		});






	});




</script>

</html>
