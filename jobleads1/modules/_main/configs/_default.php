<?php

return [
	'currency' => '€',
	'collected-taxes-multiplicator' => '1000000',


	'color-taxes-collected' => '#4444FF',
	'color-taxes-collected-avg' => '#FF0000',
	'color-tax-rates' => '#4444FF',
	'color-tax-rates-avg' => '#FF0000',

	'areas' => [
		'country-1' => [
			'name' => 'Country1',
			'type' => 'country',
		],

		'state-1' => [
			'name' => 'State1',
			'type' => 'state',
			'parent' => 'country-1',
		],

		'state-2' => [
			'name' => 'State2',
			'type' => 'state',
			'parent' => 'country-1',
		],

		'state-3' => [
			'name' => 'State3',
			'type' => 'state',
			'parent' => 'country-1',
		],

		'state-4' => [
			'name' => 'State4',
			'type' => 'state',
			'parent' => 'country-1',
		],

		'state-5' => [
			'name' => 'State5',
			'type' => 'state',
			'parent' => 'country-1',
		],

		'county-1' => [
			'name'            => 'County1',
			'type'            => 'county',
			'parent'          => 'state-1',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-2' => [
			'name'            => 'County2',
			'type'            => 'county',
			'parent'          => 'state-1',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-3' => [
			'name'            => 'County3',
			'type'            => 'county',
			'parent'          => 'state-1',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-4' => [
			'name'            => 'County4',
			'type'            => 'county',
			'parent'          => 'state-2',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-5' => [
			'name'            => 'County5',
			'type'            => 'county',
			'parent'          => 'state-2',
			'collected_taxes' => 1000,
			'tax_rate'        => 32,
		],

		'county-6' => [
			'name'            => 'County6',
			'type'            => 'county',
			'parent'          => 'state-2',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-7' => [
			'name'            => 'County7',
			'type'            => 'county',
			'parent'          => 'state-3',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-8' => [
			'name'            => 'County8',
			'type'            => 'county',
			'parent'          => 'state-3',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-9' => [
			'name'            => 'County9',
			'type'            => 'county',
			'parent'          => 'state-3',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-10' => [
			'name'            => 'County10',
			'type'            => 'county',
			'parent'          => 'state-4',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-11' => [
			'name'            => 'County11',
			'type'            => 'county',
			'parent'          => 'state-4',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-12' => [
			'name'            => 'County12',
			'type'            => 'county',
			'parent'          => 'state-4',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-13' => [
			'name'            => 'County13',
			'type'            => 'county',
			'parent'          => 'state-5',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-14' => [
			'name'            => 'County14',
			'type'            => 'county',
			'parent'          => 'state-5',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],

		'county-15' => [
			'name'            => 'County15',
			'type'            => 'county',
			'parent'          => 'state-5',
			'collected_taxes' => 100,
			'tax_rate'        => 16,
		],
	],
];
