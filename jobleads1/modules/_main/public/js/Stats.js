!function(){ 'use strict';
    var
        googleLoaded = false,
        documentReady = false;

    // call callback when document has been loaded
    $(document).ready(function(){
        documentReady = true;
        plotCharts();
    });

    // load google visualizition and call callback
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(function(){
        googleLoaded = true;
        plotCharts();
    });


    function plotCharts(){
        /**
         * Only start plotting charts, when document is ready
         * AND google visualizition has loaded.
         */
        if(!googleLoaded || !documentReady){
            return false;
        }

        var
            $graphContainers = $('.graph-container'),
            chart,
            chartData,
            data,
            options;

        // google chart options
        options = {
            'width':'100%',
            'height': '100%',
            'legend': {'position': 'none'},
            'hAxis': {'minValue': 0, 'format': ''}

        };

        // plot chart for each container
        $graphContainers.each(function(){ var $this = $(this);
            // Get chart data
            chartData = $this.data('chart_data');

            // Set chart options
            data = google.visualization.arrayToDataTable(chartData);

            // Instantiate and draw the chart
            chart = new google.visualization.BarChart(this);
            chart.draw(data, options);
        });
    }
}();

