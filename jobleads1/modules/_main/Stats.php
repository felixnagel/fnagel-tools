<?php

namespace app\modules\_main;
use Morrow\Factory;
use Morrow\Debug;

class Stats extends _Default {
	public function run(){
		// get areas from config file
		$areaDataRaw = $this->Config->get('modules._main.areas');

		// get currency and multiplicator options from config and set as template vars
		$this->Views_Serpent->setContent('currency', $this->Config->get('modules._main.currency'));
		$this->Views_Serpent->setContent('collectedTaxesMultiplicator', $this->Config->get('modules._main.collected-taxes-multiplicator'));


		/**
		 * Create all area objects and store them into the
		 * $areas array with their id's as keys.
		 */
		$areas = [];
		foreach($areaDataRaw as $areaId => $areaDatumRaw){
			// create instance
			$area = new classes\Area($areaId, $areaDatumRaw['name']);

			// set optional properties
			if(isset($areaDatumRaw['collected_taxes'])){
				$area->set_collected_taxes($areaDatumRaw['collected_taxes']);
			}
			if(isset($areaDatumRaw['tax_rate'])){
				$area->set_tax_rate($areaDatumRaw['tax_rate']);
			}

			// put into array
			$areas[$areaId] = $area;
		}

		/**
		 * Initiate area hierarchy by letting the parent areas
		 * manage their child areas.
		 */
		foreach($areaDataRaw as $areaId => $area){
			if(isset($area['parent'])){
				$areas[$area['parent']]->manage_area($areas[$areaId]);
			}
		}


		/**
		 * step 1: overall collected taxes of country 'country-1'
		 */
		$country = $areas['country-1'];
		$overallTaxes = $country->collect_taxes();

		// set template vars
		$this->Views_Serpent->setContent('overallTaxesOfCountry', $overallTaxes);
		$this->Views_Serpent->setContent('countryName', $country->get_name());


		/**
		 * step 2: create data arrays for google charts
		 */
		// chart 1: overall taxes per state of country 'country-1'
		$country = $areas['country-1'];
		$states = $country->get_managed_areas();

		// definition row
		$overallTaxesPerState = [
			['area', 'Overall taxes collected', ['role' => 'style']],
		];

		// state bars
		foreach($states as $id => $state){
			$overallTaxesPerState[] = [
				$state->get_name(),
				$state->collect_taxes(),
				$this->Config->get('modules._main.color-taxes-collected'),
			];
		}

		// average bar
		$overallTaxesPerState[] = [
			'Average value',
			round($country->collect_taxes() / count($states)),
			$this->Config->get('modules._main.color-taxes-collected-avg'),
		];

		// set template vars
		$this->Views_Serpent->setContent('overallTaxesPerStateJson', json_encode($overallTaxesPerState));
		$this->Views_Serpent->setContent('overallTaxesPerState', $overallTaxesPerState);


		// chart 2: tax rate per state of country 'country-1'
		$country = $areas['country-1'];
		$states = $country->get_managed_areas();

		// definition row
		$avgTaxRatePerState = [
			['area', 'Tax rate', ['role' => 'style']],
		];

		// state bars
		foreach($states as $id => $state){
			$avgTaxRatePerState[] = [
				$state->get_name(),
				round($state->get_tax_rate(), 2),
				$this->Config->get('modules._main.color-tax-rates'),
			];
		}

		// average bar
		$avgTaxRatePerState[] = [
			'Average value',
			round($country->get_tax_rate(), 2),
			$this->Config->get('modules._main.color-tax-rates-avg'),
		];

		// set template vars
		$this->Views_Serpent->setContent('avgTaxRatePerStateJson', json_encode($avgTaxRatePerState));
		$this->Views_Serpent->setContent('avgTaxRatePerState', $avgTaxRatePerState);


		return $this->Views_Serpent;
	}
}
