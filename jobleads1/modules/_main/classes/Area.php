<?php

namespace app\modules\_main\classes;
use Morrow\Factory;
use Morrow\Debug;

class Area extends Abstract_Area{
	/**
	 * Amount of taxes collected by this area.
	 * @var	int
	 */
	protected $_localCollectedTaxes;

	/**
	 * Tax rate of this area.
	 * @var	int
	 */
	protected $_localTaxRate;

	/**
	 * Name of this area.
	 * @var string
	 */
	protected $_name;

	/**
	 * Sets all properties on construction.
	 * @param	mixed	$id				$_id property
	 * @param 	string  $name           $_name property, optional
	 * @param 	int 	$collectedTaxes $_localCollectedTaxes property, optional
	 * @param 	int 	$taxRate        $_localTaxRate property, optional
	 */
	function __construct($id, $name = 'unknown', $collectedTaxes = 0, $taxRate = 0){
		$this->_set_id($id);
		$this->set_name($name);
		$this->set_collected_taxes($collectedTaxes);
		$this->set_tax_rate($taxRate);
	}

	/**
	 * Sets this area's name.
	 * @param	string	$name	this area's name
	 */
	public function set_name($name){
		if(!is_string($name)){
			throw new \Exception('Given parameter "name" has to be of type "string"!');
		}
		$this->_name = $name;
	}

	/**
	 * Gets this area's name.
	 * @return	string	this area's name
	 */
	public function get_name(){
		return $this->_name;
	}

	/**
	 * Gets this area's collected taxes
	 * @return 	int 	this area's collected taxes
	 */
	public function get_collected_taxes(){
		return $this->_localCollectedTaxes;
	}

	/**
	 * Sets this area's collected taxes.
	 * @param	int	$collectedTaxes	this area's collected taxes
	 */
	public function set_collected_taxes($collectedTaxes){
		if(!is_numeric($collectedTaxes)){
			throw new \Exception('Given parameter "collectedTaxes" has to be numeric!');
		}
		$this->_localCollectedTaxes = $collectedTaxes;
	}

	/**
	 * Sets this area's tax rate.
	 * @param	int	$taxRate	this area's tax rate
	 */
	public function set_tax_rate($taxRate){
		if(!is_numeric($taxRate)){
			throw new \Exception('Given parameter "taxRate" has to be numeric!');
		}
		$this->_localTaxRate = $taxRate;
	}

	/**
	 * Gets this area's tax rate. This is calculated by
	 * recursively getting all tax rates of all managed
	 * areas and creating the average value of those.
	 * @return 	int		this area's tax rate
	 */
	public function get_tax_rate(){
		$taxRates = $this->_get_tax_rates_flat_rec();
		return array_sum($taxRates) / count($taxRates);
	}

	/**
	 * Recursively collects all taxes of this area or
	 * its managed areas.
	 * @return 	int		this area's collected taxes
	 */
	public function collect_taxes(){
		$collectedTaxes = 0;

		// if this area manages other areas, collect their taxes
		if($this->_managedAreas){
			foreach($this->_managedAreas as $managedArea){
				$collectedTaxes += $managedArea->collect_taxes();
			}
		// otherwise, collect own taxes
		}else{
			$collectedTaxes += $this->_localCollectedTaxes;
		}
		return $collectedTaxes;
	}

	/**
	 * Recursively gets an array filled with all tax rates of all managed areas.
	 * @param	array	$result		array that holds the found tax rates
	 * @return 	array         		array that holds the found tax rates
	 */
	protected function _get_tax_rates_flat_rec($result = []){
		// if this area manages other areas, store their tax rates
		if($this->_managedAreas){
			foreach($this->_managedAreas as $managedArea){
				$result += $managedArea->_get_tax_rates_flat_rec($result);
			}
		// otherwise, store own tax rate
		}else{
			$result[] = $this->_localTaxRate;
		}
		return $result;
	}
}
