<?php

namespace app\modules\_main\classes;
use Morrow\Factory;
use Morrow\Debug;

abstract class Abstract_Area{
	/**
	 * Holds all areas that are managed by this one. Managed areas have
	 * must be instances of this class or a derived one. The area objects
	 * will be the values and their ids the keys.
	 * @var	array
	 */
	protected $_managedAreas = [];

	/**
	 * The area id. This id is used to manage other areas or to be managed
	 * itself. This property must be a string or numeric and can not be
	 * modified after it has been set.
	 * @var null
	 */
	protected $_id = null;

	/**
	 * Assigns the area id
	 * @param	mixed 	$id		area id
	 */
	function __construct($id){
		$this->_set_id($id);
	}

	/**
	 * Gets this area's id.
	 * @return 	mixed 	area id
	 */
	public function get_id(){
		return $this->_id;
	}

	/**
	 * Sets this area's id.
	 * @param 	mixed 	$id 	area id
	 */
	protected function _set_id($id){
		// throw an exception if given id is not string or numeric
		if(!is_string($id) && !is_numeric($id)){
			throw new \Exception('Given parameter "manageId" has to be of a string or numeric!');
		}

		$this->_id = $id;
	}

	/**
	 * Gets all areas this area manages.
	 * @return 	array 	array of managed areas
	 */
	public function get_managed_areas(){
		return $this->_managedAreas;
	}

	/**
	 * Recursively gets a specific managed area by its id. Returns false if
	 * no managed area with given area id could be found.
	 * @param	mixed	$id 	area id of desired managed area
	 * @return 	mixed			desired managed area object or false
	 */
	public function get_managed_area($id){
		// if desired area is managed by this area, return it from here
		if(isset($this->_managedAreas[$id])){
			return $this->_managedAreas[$id];
		}

		// let managed areas search for desired area and return it when found
		foreach($this->_managedAreas as $managedArea){
			$result = $managedArea->get_managed_area($id);
			if($result){
				return $result;
			}
		}

		// when no area has been found, return false
		return false;
	}

	/**
	 * Puts another area into $_managedAreas array.
	 * @param	object	$area 	area object
	 */
	public function manage_area($area){
		if(!is_subclass_of($area, __CLASS__)){
			throw new \Exception('Given parameter has to be derived from a subclass of "' . __CLASS__ . '"!');
		}

		if($this->get_managed_area($area->get_id())){
			throw new \Exception('This Area already manages another instance referring to this id!');
		}

		$this->_managedAreas[$area->get_id()] = $area;
	}
}
