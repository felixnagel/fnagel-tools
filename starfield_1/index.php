<!doctype html>

<html>
<head>
	<meta charset="utf-8" />
	<title>Starfield</title>
	<script src="easeljs-0.8.0.min.js" type="text/javascript"></script>
</head>

<body>

<canvas id="canvas" width="1800" height="800" style="background-color:#000;"></canvas>




<script type="text/javascript">

	var
	stage = new createjs.Stage('canvas'),
	stage_width = 1800,
	stage_height = 800;

	var
	methods = {
		drawStar: function drawStar(){
			var
			star = new createjs.Shape(),
			x = stage_width*Math.random() >> 0,
			y = stage_height*Math.random() >> 0,
			//n = (6*Math.random() >> 0) + 10,
			r = 1.5*Math.random();

			//star.graphics.beginFill('#fff').drawPolyStar(x, y, r, n, 0.85, 45);
			star.graphics.beginFill('#fff').drawCircle(x, y, r);
			stage.addChild(star);
		}
	};

	for(var i = 0; i <= 75; i++){
		methods.drawStar();
	}

	var t1 = new Date().getTime();
	stage.update();
	var t2 = new Date().getTime();
	console.log(t2-t1);




</script>

</body>
</html>
