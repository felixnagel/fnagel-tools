'use strict';
jQuery(document).ready(function($){
	/**
	 * collections, canvas and context vars
	 */
	var
		$topLayer                 = $('#vs-canvas-top-layer'),
		$vsAreaCanvasMain         = $('#vs-canvas-main'),
		$vsAreaCanvasOverlay      = $('#vs-canvas-overlay'),
		canvasHeight              = $vsAreaCanvasMain.height(),
		canvasWidth               = $vsAreaCanvasMain.width(),
		contextVsAreaActiveCanvas = $vsAreaCanvasOverlay[0].getContext('2d'),
		contextVsAreaCanvas       = $vsAreaCanvasMain[0].getContext('2d'),
		hoverTimeoutReference,
		imageLoader;

	/**
	 * data vars
	 */
	var
		activeVsAreaId             = false,
		backgroundImages           = $vsAreaCanvasMain.data('images_background'),
		foregroundImages           = $vsAreaCanvasMain.data('images_foreground'),
		hoverIntendDelay           = 20,
		tintColor1		           = [76, 226, 239, 255],
		vsAreaActiveImgDataObjects = {},
		vsAreaActiveUrls           = $vsAreaCanvasMain.data('vs_areas_active'),
		vsAreaImgDataObjects       = {},
		vsAreaInactiveUrls         = $vsAreaCanvasMain.data('vs_areas_inactive'),
		vsAreaUrls                 = {};


	/**
	 * functions and handlers
	 */
	var
		methods = {
			/**
			 * fills the createVsAreaActiveImgDataObjects array based on the vsAreaImgDataObjects array,
			 * then manupulates color information of these using brightenImgDataObject()
			 */
			createVsAreaActiveImgDataObjects: function createVsAreaActiveImgDataObjects(){
				//var t1 = new Date().getTime();
				for(var url in vsAreaActiveUrls){
					vsAreaActiveImgDataObjects[url] = methods.brightenImgDataObject(vsAreaImgDataObjects[url], tintColor1);
				}
				//console.log(new Date().getTime() - t1);
			},

			/**
			 * multiplies color values of each pixel in specified imgDataObject by given factor
			 */
			brightenImgDataObject: function brightenImgDataObject(imgDataObject, tintColor){
				var
					count = imgDataObject.data.length / 4,
					imgDataArrayIndex,
					i,
					j;

				var debug = [];

				// for each pixel
				for(i = 0; i < count; i++){
					imgDataArrayIndex = i * 4;
					if(
						imgDataObject.data[imgDataArrayIndex + 0] !== 255
						&&
						imgDataObject.data[imgDataArrayIndex + 1] !== 255
						&&
						imgDataObject.data[imgDataArrayIndex + 2] !== 255
						&&
						imgDataObject.data[imgDataArrayIndex + 3] !== 0
					){
						// manipulate color value
						for(j = 0; j <= 3; j++){
							imgDataArrayIndex = i * 4 + j;
							imgDataObject.data[imgDataArrayIndex] = tintColor[j];
						}
					}
				}
				return imgDataObject;
			},

			/**
			 * get the id of current vs-area, defined by a regex
			 */
			getVsIdFromVsUrl: function getVsIdFromVsUrl(vsUrl){
				return vsUrl.match(/.*(VS\d\d\d\d)\.png/)[1];
			}
		},

		handlers = {
			onActiveVsAreaIdChange: function onActiveVsAreaIdChange(event, params){
				// set new active area
				activeVsAreaId = params.in;

				// perform hover-effect with a self-clearing timeout to avoid flickering
				clearTimeout(hoverTimeoutReference);
				hoverTimeoutReference = setTimeout(function(){
					// if hovered area is active
					if(activeVsAreaId !== false){
							contextVsAreaActiveCanvas.putImageData(vsAreaActiveImgDataObjects[activeVsAreaId], 0, 0);
							$topLayer.css({'cursor': 'pointer'});
					// if hovered area is inactive
					}else{
						contextVsAreaActiveCanvas.clearRect(0, 0, canvasWidth, canvasHeight);
						$topLayer.css({'cursor': 'default'});
					}
				}, hoverIntendDelay);
			},

			onImageLoaderImageloaded: function onImageLoaderImageloaded(event, process, queueItem, queue){
				/**
				 * fill the vsAreaImgDataObjects array by drawing each single vs-area image and storing its data
				 */
				// draw this image to main canvas
				contextVsAreaCanvas.drawImage(queueItem.loadingReference, 0, 0);
				// get image data object from main canvas
				vsAreaImgDataObjects[queueItem.src] = contextVsAreaCanvas.getImageData(0, 0, canvasWidth, canvasHeight);
				// clear main canvas
				contextVsAreaCanvas.clearRect(0, 0, canvasWidth, canvasHeight);
			},

			onImageLoaderQueueCompleted: function onImageLoaderQueueCompleted(process, queue){
				// finally draw all images to main canvas
				for(var key in queue){
					contextVsAreaCanvas.drawImage(queue[key].loadingReference, 0, 0);
				}

				methods.createVsAreaActiveImgDataObjects();
			},

			onVsAreaCanvasClick: function onVsAreaCanvasClick(event){
				if(activeVsAreaId !== false){
					window.location.href = './' + vsAreaUrls[activeVsAreaId];
				}
			},

			onVsAreaCanvasMousemove: function onVsAreaCanvasMousemove(event){ var $this = $(this);
				// get mouse coords relative to canvas
				var
					offset = $this.offset(),
					mouseX = event.pageX - offset.left,
					mouseY = event.pageY - offset.top;

				/**
				 * if user does not hover any vs-area ANYMORE:
				 */
				// get color data of hovered pixel
				var
					hData = contextVsAreaCanvas.getImageData(mouseX, mouseY, 1, 1),
					hR    = hData.data[0],
					hG    = hData.data[1],
					hB    = hData.data[2],
					hA    = hData.data[3];

				/**
				 * if currently hovered pixel has a rgba value of rgba(0, 0, 0, 0) and a vsArea is currently being hovered,
				 * trigger vsHover event and interrupt this handler
				 */
				if(hR === 0 && hG === 0 && hB === 0 && hA === 0 && activeVsAreaId !== false){
					$vsAreaCanvasMain.trigger('vsHover', {'out': activeVsAreaId, 'in': false});
					return false;
				}

				/**
				 * if user hovers ANOTHER vs-area:
				 */
				// calculate the index of the img data array of currently hovered pixel
				var imgDataArrayIndex  = (mouseY * canvasWidth + mouseX) * 4;

				/**
				 * check each active vs-area image data array
				 * if the hovered pixel has any other value than rgba(0, 0, 0, 0) and is not one of the vs-area that is currently hovered,
				 * trigger vsHover event
				 */
				var hoveringAnyVsArea = false;
				// for each active vs-area
				for(var vsAreaId in vsAreaActiveImgDataObjects){
					var
						dR = vsAreaActiveImgDataObjects[vsAreaId].data[imgDataArrayIndex],
						dG = vsAreaActiveImgDataObjects[vsAreaId].data[imgDataArrayIndex + 1],
						dB = vsAreaActiveImgDataObjects[vsAreaId].data[imgDataArrayIndex + 2],
						dA = vsAreaActiveImgDataObjects[vsAreaId].data[imgDataArrayIndex + 3];

					// if there is any active area at this point
					if(dR !== 0 || dG !== 0 || dB !== 0 || dA !== 0){
						// set flag
						hoveringAnyVsArea = true;
						// if the currently hovered area is not the previously hovered one
						if(activeVsAreaId !== vsAreaId){
							// trigger specific vsHover event
							$vsAreaCanvasMain.trigger('vsHover', {'out': activeVsAreaId, 'in': vsAreaId});
							// no need to search any further
							break;
						}
					}
				}
				// if no area has been found
				if(hoveringAnyVsArea === false){
					// trigger specific vsHover event
					$vsAreaCanvasMain.trigger('vsHover', {'out': activeVsAreaId, 'in': false});
				}
			}
		};

	/**
	 * setup event listeners
	 */
	$topLayer.on('click', handlers.onVsAreaCanvasClick);
	$topLayer.on('mousemove', handlers.onVsAreaCanvasMousemove);
	$vsAreaCanvasMain.on('vsHover', handlers.onActiveVsAreaIdChange);

	/**
	 * initial script
	 */
	vsAreaUrls = $.extend(vsAreaInactiveUrls, vsAreaActiveUrls);

	var vsAreaUrlsArray = [];
	for(var url in vsAreaUrls){
		vsAreaUrlsArray.push(url);
	}

	// create image loader instance and start loading images
	imageLoader = new $.FnagelAsynchronousImageLoader({
		urls: vsAreaUrlsArray,
		onImageLoaded: handlers.onImageLoaderImageloaded,
		onQueueCompleted: handlers.onImageLoaderQueueCompleted
	});
	imageLoader.start();
});
