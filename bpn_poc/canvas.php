<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<title>foo</title>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="css.css">



</head>
<body>
	<canvas
		class="vs-canvas"
		id="vs-canvas-main"
		width="690"
		height="976"
		data-vs_areas_inactive="<?php echo htmlspecialchars(json_encode([
			'images/VS grau/VS0101.png' => 'foo',
			'images/VS grau/VS0103.png' => 'foo',
			'images/VS grau/VS0191.png' => 'foo',
			'images/VS grau/VS0192.png' => 'foo',
			'images/VS grau/VS0261.png' => 'foo',
			'images/VS grau/VS0492.png' => 'foo',
			'images/VS grau/VS0911.png' => 'foo',
			'images/VS grau/VS0931.png' => 'foo',
			'images/VS grau/VS0951.png' => 'foo',
			'images/VS grau/VS0954.png' => 'foo',
		]));?>"
		data-vs_areas_active ="<?php echo htmlspecialchars(json_encode([
			'images/VS blau/VS0263.png' => 'foo',
			'images/VS blau/VS0291.png' => 'foo',
			'images/VS blau/VS0301.png' => 'foo',
			'images/VS blau/VS0302.png' => 'foo',
			'images/VS blau/VS0321.png' => 'foo',
		]));?>"
	>
	</canvas>
	<canvas
		class="vs-canvas"
		id="vs-canvas-overlay"
		width="690"
		height="976"
	>
	</canvas>

	<div id="vs-canvas-top-layer" style="width:690px; height:976px;"></div>


</body>

<script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
<!--[if IE]><script src="excanvas.compiled.js"></script><![endif]-->
<script type="text/javascript" src="../js/fnagel-asynchronous-image-loader.js"></script>
<script type="text/javascript" src="js.js"></script>


</html>
