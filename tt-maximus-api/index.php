<!doctype html>
<html>
<head>
	<title>foo</title>
	<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
	<style>
		.htv-table-container{float:left; display:inline-block; text-align:center; vertical-align:top; padding:0 2px; box-sizing:border-box; font-family:verdana, sans-serif;}
		.htv-table-container h2{font-size:20px;}
		.htv-table{width:100%; border-collapse: collapse; font-size:13px; display:none;}
		.htv-table th{background-color:#D90000; color:#FFFFFF;}
		.htv-table tr{background-color:#FCFCFC;}
		.htv-table tr.hoverable{cursor:pointer;}
		.htv-table tr.hoverable:hover td{background-color:#EAEAEA;}
		.htv-table tr.odd{background-color:#F4F4F4;}
		.htv-table 	td.bold{font-weight:bold;}
		.htv-table 	td.htv-neutral{color:#FF7E21;}
		.htv-table 	td.htv-positive{color:#00D900;}
		.htv-table 	td.htv-negative{color:#D90000;}
		.htv-table td, .htv-table th{padding:6px 0;}
	</style>
</head>

<body>


<div class="htv-table-container" style="width:24%;">
	<h2>Platzierungen</h2>
	<table class="htv-table" id="htv-table">
		<thead>
			<tr>
				<th>Mannschaft</th>
				<th>Liga</th>
				<th>Platz</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<div class="htv-table-container" style="width:41%;">
	<h2>Aktuelle Spielergebnisse</h2>
	<table class="htv-table" id="htv-recent-matches">
		<thead>
			<tr>
				<th>Heim</th>
				<th>Gast</th>
				<th>Datum</th>
				<th>Ergebnis</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<div class="htv-table-container" style="width:35%;">
	<h2>Nächste Termine</h2>
	<table class="htv-table" id="htv-upcoming-matches">
		<thead>
			<tr>
				<th>Heim</th>
				<th>Gast</th>
				<th>Datum</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>



<script type="text/javascript">
!function(){
	var
	interval_reference = setInterval(function(){
		if(typeof jQuery !== 'undefined'){
			init(jQuery);
			clearInterval(interval_reference);
		}
	}, 100);

	function init($){
		$(document).ready(function(){ 'use-strict';
			var matches_display_number = 10;

			$.ajax({
				url: 'http://www.tt-maximus.de/pdpages/ttmaximus/include/TtmaxJsonPunktspiele.php?code=a9d3b5ad5cade993e537dc01f2ac6030&sg=1',
			}).done(function(data){
				var matches = methods.extract_matches(JSON.parse(data));

				// SPIELERGEBNISSE
				var $container = $('#htv-recent-matches tbody'),
					recent_matches = methods.get_recent_matches(matches, matches_display_number);

				for(var i in recent_matches){
					var match = recent_matches[i],
						odd_class = i % 2 ? 'odd' : 'even' ;

					$container.append('<tr class="'+odd_class+' hoverable"><td>'+match.heim+'</td><td>'+match.gast+'</td><td>'+match.datum+'</td><td class="'+match.css+' bold">'+match.erg+'</td></tr>');

					var	link = 'http://www.tt-maximus.de/pdpages/ttmaximus/herren_punktspielbericht.php?page=erg1&stnr='+encodeURIComponent(match.st)+'&stname='+encodeURIComponent(match.staffel)+'&spnr='+encodeURIComponent(match.spnr);
					methods.add_click_link_listener($container.find('tr').last(), link);
				}
				$container.parent().show();

				// AUSSTEHENEDE SPIELE
				var $container = $('#htv-upcoming-matches tbody'),
					upcoming_matches = methods.get_upcoming_matches(matches, matches_display_number);

				for(var i in upcoming_matches){
					var match = upcoming_matches[i],
						odd_class = i % 2 ? 'odd' : 'even' ;

					$container.append('<tr class="'+odd_class+'"><td>'+match.heim+'</td><td>'+match.gast+'</td><td class="bold">'+match.datum+'</td></tr>');
				}
				$container.parent().show();
			});

			$.ajax({
				url: 'http://www.tt-maximus.de/pdpages/ttmaximus/include/TtmaxJsonTabellen.php?code=a9d3b5ad5cade993e537dc01f2ac6030&sg=1',
			}).done(function(data){
				var teams = methods.extract_teams(JSON.parse(data));

				// TABELLE
				var $container = $('#htv-table tbody'),
					count = 0;

				for(var team_name in teams){
					var team = teams[team_name],
						odd_class = count++ % 2 ? 'odd' : 'even' ;

					$container.append('<tr class="'+odd_class+' hoverable"><td>'+team_name+'</td><td>'+team.liga.staffelname+'</td><td class="'+team.css+' bold">'+team.platz+'</td></tr>');

					var	link = 'http://www.tt-maximus.de/pdpages/ttmaximus/herren_staffeln.php?page=tab&stnr='+encodeURIComponent(team.liga.staffelid)+'&stname='+encodeURIComponent(team.liga.staffelname);
					methods.add_click_link_listener($container.find('tr').last(), link);
				}
				$container.parent().show();
			});

			var
			methods = {
				add_click_link_listener: function add_click_link_listener($element, link){
					$element.on('click', function(){
						$('<a href="'+link+'" target="_blank"></a>')[0].click();
					});
				},

				extract_teams: function extract_teams(data){
					var teams_temp = {},
						teams_temp_keys = [];

					for(var i in data){
						var league = data[i].staffel,
							rankings = league.tabelle;

						for(var j in rankings){
							var team = rankings[j];

							// ignore other teams
							if(team.vereinnr !== '0730'){
								continue;
							}

							// set league as member
							team.liga = league;

							// avoid infinite ref
							delete team.liga.tabelle;

							// set css class
							if(team.platz <= parseInt(league.aufsteiger, 10)){
								team.css = 'htv-positive';
							}
							if(team.platz >= rankings.length - parseInt(league.absteiger, 10)){
								team.css = 'htv-negative';
							}

							// put into temp json, because it needs to get sorted
							var team_name = team.vereinname+' '+team.manr;
							teams_temp[team_name] = team;
							teams_temp_keys.push(team_name);
						}
					}

					// sort by keys and put into final json
					teams_temp_keys.sort();
					var teams = {};
					for(var i in teams_temp_keys){
						var key = teams_temp_keys[i];
						teams[key] = teams_temp[key];
					}

					return teams;
				},

				extract_matches: function extract_matches(data){
					var matches = [];

					// put matches with their leagues in array
					for(var i in data){
						var mannschaft = data[i].mannschaft;
						for(var j in mannschaft.pktsp){
							var match = mannschaft.pktsp[j];
							match.staffel = mannschaft.staffel;
							matches.push(match);
						}
					}

					// set ts of each match
					for(var i in matches){
						var match = matches[i],
							pm_date = match.datum.match(/^(\d+)\.(\d+)\.(\d+)$/),
							pm_time = match.zeit.match(/^(\d+):(\d+)$/),
							date = new Date(pm_date[3], pm_date[2]-1, pm_date[1], pm_time[1], pm_time[2]);

						match.ts = date.getTime();
					}

					return matches;
				},

				get_recent_matches: function get_recent_matches(matches, n){
					var recent_matches = [],
						now = (new Date()).getTime();

					// sort descending by ts
					matches.sort(function(a, b){
						return b.ts - a.ts;
					});

					// fetch first n matches and set css classes
					for(var i in matches){
						var match = matches[i];

						if(match.ts < now){
							var pm_score = match.erg.match(/^(\d+):(\d+)$/),
								score_local = parseInt(pm_score[1], 10),
								score_guest = parseInt(pm_score[2], 10),
								is_local = match.heim.match(/^horner tv \d+$/i) ? true : false;

							match.css = 'htv-neutral';
							if(score_local > 8 && is_local || score_guest > 8 && !is_local){
								match.css = 'htv-positive';
							}
							if(score_local < 8 && is_local || score_guest < 8 && !is_local){
								match.css = 'htv-negative';
							}

							recent_matches.push(match);
						}

						if(recent_matches.length >= n){
							break;
						}
					}

					return recent_matches;
				},

				get_upcoming_matches: function get_upcoming_matches(matches, n){
					var upcoming_matches = [],
						now = (new Date()).getTime();

					// sort ascending by ts
					matches.sort(function(a, b){
						return a.ts - b.ts;
					});

					// fetch first n matches and set css classes
					for(var i in matches){
						if(matches[i].ts > now){
							upcoming_matches.push(matches[i]);
						}
						if(upcoming_matches.length >= n){
							break;
						}
					}

					return upcoming_matches;
				}
			};
		});
	}
}();
</script>

</body>
</html>


