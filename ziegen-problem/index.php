<?php
$keep_decision = false;

$wins = 0;
$games = 0;
$iterations = 1000000;
while($iterations--){
	// shuffle doors
	$doors = [true, false, false];
	shuffle($doors);

	// decide
	$initial_decision = rand(0, 2);

	// open a door
	foreach($doors as $door => $is_win){
		if($initial_decision !== $door && !$is_win){
			unset($doors[$door]);
			break;
		}
	}

	// do second decision
	$second_decision;
	if($keep_decision === false){
		foreach($doors as $door => $is_win){
			// get unopened wrong door
			if($door != $initial_decision){
				$second_decision = $door;
				break;
			}
		}
	}else{
		// keep the door
		$second_decision = $initial_decision;
	}

	$games++;

	if($doors[$second_decision]){
		$wins++;
	}
}

echo '<p>Keep decision</p>';
echo '<pre>';
var_dump($keep_decision);
echo '</pre><br>';

echo '<p>Games</p>';
echo '<pre>';
var_dump($games);
echo '</pre><br>';


echo '<p>Wins</p>';
echo '<pre>';
var_dump($wins);
echo '</pre><br>';

echo '<p>Winrate</p>';
echo '<pre>';
var_dump($wins/$games);
echo '</pre><br>';
