function Heart(scale, speed, canvas_width, canvas_height){
	var self = this;

	self.scale = scale*Math.random() + 1;
	self.speed = speed*self.scale;
	self.canvas_width = canvas_width;
	self.canvas_height = canvas_height;

	var
		heart = new createjs.Shape(),
		r = self._(255, 0.5),
		g = self._(50, 1),
		b = self._(50, 1);

	heart.graphics.beginFill(createjs.Graphics.getRGB(r, g, b));
	heart.graphics.moveTo(0, -12);
	heart.graphics.curveTo(1, -20, 8, -20);
	heart.graphics.curveTo(16, -20, 16, -10);
	heart.graphics.curveTo(16, 0, 0, 12);
	heart.graphics.curveTo(-16, 0, -16, -10);
	heart.graphics.curveTo(-16, -20, -8, -20);
	heart.graphics.curveTo(-1, -20, 0, -12);
	self.element = heart;

	self.element.scaleX = self.scale;
	self.element.scaleY = self.scale;

	self.amplitude_x = self._(100, 0.7);
	self.frequency = 0.001*Math.random();

	self.base_x = self.canvas_width*Math.random() - 0.5*self.element.scaleX*20;
	self.element.y = self.canvas_height*Math.random() - 0.5*self.element.scaleY*20;

	self.current_speed_x = 0;

	self.phi = 2*Math.PI*Math.random();

	$(window).on('mousemove', function(event){
		if(self.mouse_position_x){
			self.base_x += (event.pageX-self.mouse_position_x)*self.scale / self.canvas_width*40;
			self.element.y += (event.pageY-self.mouse_position_y)*self.scale / self.canvas_height*20;
		}

		self.mouse_position_x = event.pageX;
		self.mouse_position_y = event.pageY;
	});
}

Heart.prototype = {
	canvas_width: null,
	canvas_height: null,
	mouse_position_x: null,
	mouse_position_y: null,

	step: function(){
		this.phi += this.frequency;
		this.phi %= 2;
		this.element.x = this.base_x + this.amplitude_x*Math.sin(this.phi*Math.PI);
		this.element.y += this.speed;

		if(this.element.y > this.canvas_height + 20*this.scale){
			this.base_x = this.canvas_width*Math.random();
			this.element.y = -20*this.scale;
		}
	},

	_: function(x, p){
		p = !p ? 0.2 : p;
		return (x*p*Math.random()+x*(1-p))>>0;
	}
};
