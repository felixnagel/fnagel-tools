$('document').ready(function(){ 'use strict';
	var
		$window = $(window),
		$canvas = $('#canvas_1'),
		canvas  = $canvas[0],
		ctx     = canvas.getContext('2d'),
		_Stage  = new createjs.Stage(canvas),
		i       = 0,
		n       = 30,
		hearts  = [];

	canvas.width = $window.innerWidth();
	canvas.height = $window.innerHeight();

	for(i = 0; i < n; i++){
		hearts.push(new Heart(3, 0.05, canvas.width, canvas.height));
	}
	hearts.sort(function(a, b){
		return a.scale - b.scale;
	});

	for(i = 0; i < n; i++){
		_Stage.addChild(hearts[i].element);
	}

	createjs.Ticker.addEventListener("tick", handleTick);
	createjs.Ticker.useRAF = false;
	createjs.Ticker.setFPS(1000);

 	function handleTick(event) {
 		for(i in hearts){
 			hearts[i].step();
 		}
    	_Stage.update();
 	}
});
