$('document').ready(function(){ 'use strict';

	var
		_data = $('#panel-container').data('content'),
		_can_read = false,
		$panel_container = $('#panel-container'),
		$panel_trigger = $('#panel-container .trigger-voice'),
		$text = $('#text'),
		_message = new SpeechSynthesisUtterance(),
		voiceSettings = {volume: 1, rate: 1, pitch: 0.7};


	var
		methods = {
			createText: function createText(type){
				var
					name_1   = methods.getRandomKey(_data.kosenamen),
					gender_1 = _data.kosenamen[name_1],
					adj_1    = methods.getRandomValue(_data.adjektive[gender_1]),
					anrede_1 = gender_1 == 'w' ? 'Meine' : 'Mein';

				delete _data.kosenamen[name_1];

				var
					content  = methods.getRandomValue(_data.inhalt[type]),
					name_2   = methods.getRandomKey(_data.kosenamen),
					gender_2 = _data.kosenamen[name_2],
					anrede_2 = gender_2 == 'w' ? 'Deine' : 'Dein';

				return anrede_1 + ' ' + adj_1 + ' ' + name_1 + ', ' + content + ' ' + anrede_2 + ' ' + name_2 + '.';
			},

			getRandomKey: function getRandomKey(obj){
				var
					props = Object.keys(obj),
					key = Math.floor(props.length*Math.random());
				return props[key];
			},

			getRandomValue: function getRandomValue(obj){
				return obj[methods.getRandomKey(obj)];
			},

			readMessage: function readMessage(text){
				_can_read = false;
				_message.text = text;
				window.speechSynthesis.speak(_message);
			},

			showText: function showText(text){
				$text.html(text);
				$text.addClass('visible');
			},

			hideText: function hideText(){
				$text.removeClass('visible');
			}
		};

	for(var i in voiceSettings){
		_message[i] = voiceSettings[i];
	}
	var
		setupInterval = setInterval(function(){
			var voices = window.speechSynthesis.getVoices();

			if(voices.length){
				_message.voice = voices[voiceSettings.voice];
				$(window).trigger('voice-can-read');
				clearInterval(setupInterval);
			}
		}, 100);

	_message.onend = function(){
		$(window).trigger('voice-can-read');
		methods.hideText();
	};

	$(window).on('voice-can-read', function(){
		_can_read = true;
	});

	$panel_trigger.on('click', function(){ var $this = $(this);
		if(!_can_read){
			return false;
		}

		var text = methods.createText($this.data('mode'));
		methods.readMessage(text);
		methods.showText(text);
	});

	$panel_trigger.hover(function(){
		if(!_can_read){
			return false;
		}
		methods.showText($(this).data('mode'));
	}, function(){
		if(!_can_read){
			return false;
		}
		methods.hideText();
	});






});

