<?php

$_data = [
	"kosenamen" => [
		"Adonis"                 => "m",
		"Bär"                    => "m",
		"Ein und Alles"          => "s",
		"Gottgegebenes Geschenk" => "s",
		"Hase"                   => "m",
		"Hasilein"               => "s",
		"Herz"                   => "s",
		"Herzblatt"              => "s",
		"Kirsche"                => "w",
		"Knuffel"                => "s",
		"Kuschelbatzen"          => "m",
		"Liebling"               => "m",
		"Mausebärchen"           => "s",
		"Mausi"                  => "s",
		"Purzel"                 => "m",
		"Raubkätzchen"           => "s",
		"Schatzi"                => "m",
		"Schnuffel Puffel"       => "m",
		"Sinn des Lebens"        => "m",
		"Tiger"                  => "m",
		"Zuckerschnecke"         => "w",
	],

	"adjektive" => [
		"m" => [
			"anbetungswürdiger",
			"herrlicher",
			"hübscher",
			"knuffiger",
			"lieblicher",
			"liebster",
			"perfekter",
			"süßer",
			"wunderschöner",
			"wundervoller",
			"zarter",
		],
		"s" => [
			"anbetungswürdiges",
			"herrliches",
			"hübsches",
			"knuffiges",
			"liebliches",
			"liebstes",
			"perfektes",
			"süßes",
			"wunderschönes",
			"wundervolles",
			"zartes",
		],
		"w" => [
			"anbetungswürdige",
			"herrliche",
			"hübsche",
			"knuffige",
			"liebliche",
			"liebste",
			"perfekte",
			"süße",
			"wunderschöne",
			"wundervolle",
			"zarte",
		],
	],

	"inhalt" => [
		"Kompliment" => [
			"Dein Lachen ist das schönste Geräusch, was meine Ohren jemals zu hören vermocht haben.",
			"Deine Stimme hallt wie Engelsgesang auf der irdischen Welt.",
			"Die Welt erblasst bei deiner Schönheit.",
			"Du bist so toll! Ich liebe dich.",
			"Ich erstarre in tiefster Demut im Antlitz deiner Schönheit.",
			"Kein Ozean ist so tief und blau wie deine Augen.",
			"Kein Satz dieser Welt, kein Gedicht, ja - nicht mal ein Buch könnte deine vollendete Pracht beschreiben.",
			"Was habe ich nur die ganze Zeit getan, bevor ich dich getroffen habe?",
			"Welch unsägliches Glück es ist, dich kennen zu dürfen.",
		],
		"Sehnsucht" => [
			"Die Welt ist grau und kalt ohne dich.",
			"Ich möchte nie wieder ohne dich sein.",
			"Ich vermisse dich so sehr.",
			"Ich wünschte, du wärest jetzt hier.",
			"Ich wünschte, ich könnte jetzt in deiner Nähe sein.",
			"In Gedanken bist du immer bei mir.",
			"Meine Gedanken kreisen nur um dich.",
			"Ohne dich ist die Welt so farblos.",
			"Wenn du weg bist, fehlt ein Teil von mir.",
		],
	],
];

function text($_data, $entscheidung){
	/*
	echo '<pre>';
	var_dump($_data);
	echo '</pre>';
	*/

	$kosename1 = array_rand($_data["kosenamen"]);
	$geschlecht1 = $_data["kosenamen"][$kosename1];
	$anrede = ($geschlecht1 == "m" || $geschlecht1 == "s") ? "Mein" : "Meine";

	$adjektiv_key = array_rand($_data["adjektive"][$geschlecht1]);
	$adjektiv = $_data["adjektive"][$geschlecht1][$adjektiv_key];

	$inhalt_key = array_rand($_data["inhalt"][$entscheidung]);
	$inhalt = $_data["inhalt"][$entscheidung][$inhalt_key];

	$kosename2 = array_rand($_data["kosenamen"]);
	$geschlecht2 = $_data["kosenamen"][$kosename2];
	$verabschiedung = ($geschlecht2 == "m" || $geschlecht2 == "s") ? "Dein" : "Deine";

	return $anrede." ".$adjektiv." ".$kosename1.", ".$inhalt." ".$verabschiedung." ".$kosename2.".";
}
