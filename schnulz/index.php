<?php
	include('content.php');
?>

<!doctype html>
<html>
	<head>
		<title>Schnulz</title>
		<meta charset="utf-8" />
		<link rel="shortcut icon" href="favicon.ico" />

		<link rel="stylesheet" href="css/style.css" />

		<link href='https://fonts.googleapis.com/css?family=Tangerine:700' rel='stylesheet' type='text/css' />

		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="js/easeljs-0.8.0.min.js"></script>
		<script type="text/javascript" src="js/Heart.js"></script>
		<script type="text/javascript" src="js/animated_hearts.js"></script>
		<script type="text/javascript" src="js/index.js"></script>
	</head>

	<body>
		<canvas id="canvas_1"></canvas>

		<div id="panel-container" data-content="<?php echo htmlspecialchars(json_encode($_data)); ?>">
			<div class="trigger-voice-container">
				<div class="trigger-voice" data-mode="Kompliment">
					<img src="images/herz.png" />
				</div>
			</div>
			<div class="trigger-voice-container">
				<div class="trigger-voice" data-mode="Sehnsucht">
					<img src="images/herz.png" />
				</div>
			</div>

			<div id="text-container">
				<p id="text"></p>
			</div>


		</div>
	</body>
</html>



