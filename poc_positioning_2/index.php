<!doctype html>
<html>
<head>
	<style>
	#canvas{margin:0 auto; display:block; border:1px solid #000000;}
	#left-div{width:45%; display:inline-block;}
	#right-div{width:45%; display:inline-block;}

	</style>
</head>
<body>

	<div id="left-div">
		<canvas id="canvas" width="800" height="600"></canvas>
	</div>

	<div id="right-div">
		<input id="rotation" type="range" min="0" max="2" step="0.02" value="0" />
		<input id="rotation-auto" type="button" />
	</div>
</body>

<script src="../js/jquery-2.1.4.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(function(){

	var
		$canvas = $('#canvas'),
		ctx     = $canvas[0].getContext('2d');

	var
		pwWidth  = $canvas.width(),
		pwHeight = $canvas.height();

	ctx.fillStyle   = '#FFAAAA';
	ctx.strokeStyle = '#000000';
	ctx.lineWidth   = 1;

	var
		sA = {
			w: 100,
			h: 200,
			x: 400,
			y: 300,
			rot: 0
		},
		sR = {
			w: 0,
			h: 500,
			x: 0,
			y: 0,
			rot: 0
		};



	function rad(deg){
		return deg/180*Math.PI;
	}

	function getCoords(sq){
		var
			p1 = [
				(-0.5*sq.w)*Math.cos(sq.rot) + (-0.5*sq.h)*Math.sin(sq.rot) + sq.x,
				(-0.5*sq.w)*-Math.sin(sq.rot) + (-0.5*sq.h)*Math.cos(sq.rot) + sq.y
			];
			p2 = [
				(0.5*sq.w)*Math.cos(sq.rot) + (-0.5*sq.h)*Math.sin(sq.rot) + sq.x,
				(0.5*sq.w)*-Math.sin(sq.rot) + (-0.5*sq.h)*Math.cos(sq.rot) + sq.y
			];
			p3 = [
				(0.5*sq.w)*Math.cos(sq.rot) + (0.5*sq.h)*Math.sin(sq.rot) + sq.x,
				(0.5*sq.w)*-Math.sin(sq.rot) + (0.5*sq.h)*Math.cos(sq.rot) + sq.y
			];
			p4 = [
				(-0.5*sq.w)*Math.cos(sq.rot) + (0.5*sq.h)*Math.sin(sq.rot) + sq.x,
				(-0.5*sq.w)*-Math.sin(sq.rot) + (0.5*sq.h)*Math.cos(sq.rot) + sq.y
			];
		return [p1, p2, p3, p4];
	}

	function clearCanvas(){
		ctx.clearRect(0, 0, pwWidth, pwHeight);
	}

	function drawSquare(sq){
		ctx.beginPath();

		var points = getCoords(sq);

		ctx.moveTo(points[0][0] + 0.5, points[0][1] + 0.5);

		for(var key in points){
			ctx.lineTo(
				points[key][0] + 0.5,
				points[key][1] + 0.5
			);
		}

		ctx.closePath();
		ctx.fill();
		ctx.stroke();
	}

	function sq(val){
		return val*val;
	}

	function getAlpha(phi){
		var
			gamma,
			phi;

		phi %= (2*Math.PI);

		if(phi >= 0.0*Math.PI && phi < 0.5*Math.PI || phi >= 1.0*Math.PI && phi < 1.5*Math.PI){
			gamma = Math.atan(sA.h/sA.w);
		}
		if(phi >= 0.5*Math.PI && phi < 1.0*Math.PI || phi >= 1.5*Math.PI && phi < 2.0*Math.PI){
			gamma = Math.atan(sA.w/sA.h);
		}
		return gamma - (phi % (0.5*Math.PI));
	}

	function positionR(phi){
		var
			alpha,
			r;

		alpha = getAlpha(phi);
		r = Math.sqrt(sq(0.5*sA.w) + sq(0.5*sA.h)) * Math.cos(alpha);
		sR.x = sA.x + Math.round(r*Math.cos(phi));
		sR.y = sA.y + Math.round(r*Math.sin(phi));
		sR.rot = -phi;

		clearCanvas();
		drawSquare(sA);
		drawSquare(sR);
	}

	$('#rotation').on('change mousemove', function(){ $this = $(this);
		positionR($this.val()*Math.PI);
	});

	$('#rotation-auto').on('click', function(){
		var autoPhi = 0;
		setInterval(function(){
			autoPhi += 0.003*Math.PI;
			positionR(autoPhi);
		}, 1);
	});


});




</script>



</html>
