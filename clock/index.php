<!doctype html>
<html>
<head>
	<style>
	</style>
</head>
<body>
	<div id="clock"></div>

	<button id="start">Start</button>
	<button id="stop">Stop</button>
	<button id="reset">Reset</button>
	<button id="set">Set</button>



</body>

<script src="../js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="../js/fnagel-chrono_jquery-plugin.js" type="text/javascript"></script>


<script type="text/javascript">
	var $clock = $('#clock');

	$clock.on('fnagelChrono1-tick', function(event, tick){
		$clock.html(tick.part);
	});

	$clock.fnagelChrono1({
		tickInterval: 10,
		returnMilliseconds: true
	});

	$clock.trigger('fnagelChrono1-reset');


	$('#start').on('click', function(){
		$clock.trigger('fnagelChrono1-start');
	});
	$('#stop').on('click', function(){
		$clock.trigger('fnagelChrono1-stop');
	});
	$('#reset').on('click', function(){
		$clock.trigger('fnagelChrono1-reset');
	});
	$('#set').on('click', function(){
		$clock.trigger('fnagelChrono1-set', {h: 13, m: 37});
	});





</script>


</html>
