function glossy(selector, duration, glossy_width, rotation_deg) {
	var
		canvas = document.createElement('canvas'),
		ctx    = canvas.getContext('2d'),
		image  = document.querySelector(selector),
		w      = image.clientWidth,
		h      = image.clientHeight;

	var
		step     = 1 / (duration/60),
		progress = 0;

	var
		// closer coordinate of glossy at the start of animation
		p1_start,
		// farer coordinate of glossy at the start of animation
		p2_start,
		// former closer coordinate of glossy, but at the end of animation the farer one
		p1_end,
		// former farer coordinate of glossy, but at the end of animation the closer one
		p2_end;

	var
		// transfroms polar coordinates to cartesian coordinates
		polarToCartesian = function polarToCartesian(r, phi){
			return {x: r*Math.cos(phi), y: r*Math.sin(phi)};
		},
		// transforms a gegree angle to a radian one, values > 2*PI will be normalized
		degToRad = function degToRad(deg){
			return normalizeRad(deg/180*Math.PI);
		},
		// transforms radian angle values to normalized ones
		normalizeRad = function normalizeRad(rad){
			return rad % (2*Math.PI);
		},
		// adds any linear offset to any cartesian coordinates
		addOffset = function addOffset(p, offset_x, offset_y){
			return {x: p.x + offset_x, y: p.y + offset_y};
		},
		// calculates the minimum starting distance of the glossy rectangle from the image
		getR = function getR(phi, w, h){
			phi = normalizeRad(phi);

			var gamma;
			if(phi >= 0 && phi < 0.5*Math.PI || phi >= Math.PI && phi < 1.5*Math.PI){
				gamma = Math.atan(h/w);
			}else{
				gamma = Math.atan(w/h);
			}

			var alpha = gamma - (phi % (0.5*Math.PI));
			return Math.sqrt(Math.pow(0.5*w, 2) + Math.pow(0.5*h, 2)) * Math.cos(alpha);
		},
		// render the effect
		render = function render(){
			// clear canvas
			ctx.clearRect(0, 0, canvas.width, canvas.height);

			// increase progress
			progress += step;

			// calc current positions
			var
				p1_current = {x: p1_start.x + (p1_end.x-p1_start.x)*progress, y: p1_start.y + (p1_end.y-p1_start.y)*progress},
				p2_current = {x: p2_start.x + (p2_end.x-p2_start.x)*progress, y: p2_start.y + (p2_end.y-p2_start.y)*progress};

			// create gradient
			var gradient  = ctx.createLinearGradient(p1_current.x, p1_current.y, p2_current.x, p2_current.y);
			gradient.addColorStop(0.0, 'rgba(255,255,255,0.0)');
			gradient.addColorStop(0.5, 'rgba(255,255,255,0.7)');
			gradient.addColorStop(1.0, 'rgba(255,255,255,0.0)');
			ctx.fillStyle = gradient;
			ctx.fillRect(0, 0, canvas.width, canvas.height);

			// copy the ring into the image
			//ctx.globalCompositeOperation = "destination-over"; // if you want the boring version
			ctx.globalCompositeOperation = "lighter"; // the cooler version
			ctx.drawImage(image, 0,0);

			// mask the composition with the ring
			ctx.globalCompositeOperation = "destination-atop";
			ctx.drawImage(image, 0,0);

			// call itself until progress hit 1
			if(progress < 1){
				requestAnimationFrame(render);
			}
		};

	// create canvas and set its dimensions
	document.body.appendChild(canvas);
	canvas.width  = w;
	canvas.height = h;

	/**
	 * Calculate polar angle and distances of glossy positions (polar coordinates).
	 *
	 * r1: disance of glossy start (close to image)
	 * r1: disance of glossy end (far from image)
	 */
	var
		phi      = degToRad(rotation_deg),
		r1       = getR(phi, w, h),
		r2       = r1 + glossy_width;

	/**
	 * Now transform those polar coordinates to cartesian coordinates
	 * and add offsets. Those offsets are bothe the halves of the image's
	 * dimensions. This is done because the positioning is relative to the
	 * image's center point. Note that p1_end will be assigned with the
	 * disance r2, since the closer part of the glossy rectangle will
	 * be the farer one when the animation has finished.
	 */
	p1_start = polarToCartesian(r1, phi),
	p1_start = addOffset(p1_start, 0.5*w, 0.5*h);

	p2_start = polarToCartesian(r2, phi),
	p2_start = addOffset(p2_start, 0.5*w, 0.5*h);

	p1_end   = polarToCartesian(r2, normalizeRad(phi + Math.PI)),
	p1_end = addOffset(p1_end, 0.5*w, 0.5*h);

	p2_end   = polarToCartesian(r1, normalizeRad(phi + Math.PI));
	p2_end = addOffset(p2_end, 0.5*w, 0.5*h);

	// init rendering
	render();
}
