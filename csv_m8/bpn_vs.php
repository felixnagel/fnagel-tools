<?php
/**
 * Felix Nagel
 * 05.06.2015
 */


/**
 * update table "vertriebsstellen" (CURRENT DATA) with this script and a table mainted by bpn (NEW DATA)
 *
 * specifications:
 * table layouts:
 *
 * CURRENT DATA:
 * [ id | MVS name | MVS id | VS id | MVS active flag | VS active flag ]
 * example:
 * [ 1 | MVS Dresden | '' | 01 | 0 | 0 ]
 * [ 655 | MVS Weimar | 98 | 0656 | 1 | 0 ]
 *
 * MVS id is a foreign key to the very self table for some reason
 * empty MVS id string means it is the MVS record itself
 * non-empty MVS id string means it is a VS record
 *
 * NEW DATA:
 * [ PLZ | VS id | MVS id | MVS name ]
 * example:
 * [ 01159 | VS0101 | MVS01 | MVS Dresden ]
 *
 * ignore PLZ, its irrelevant
 */

/**
 * init helper class
 */

/* for usage remove comment here  <-----------

include('ParseCSV.php');
$csvParser = new ParseCSV([
	'delimiter' => ',',
	'enclosure' => '"',
	'escape'    => '\\',
]);


// read csv files
$csvParser->read('csv_files/new_data.csv');
$sheet = $csvParser->getContent();
array_pop($sheet);

$csvParser->read('csv_files/current_data.csv');
$dbCurrent = $csvParser->getContent();
array_pop($dbCurrent);

// CURRENT DATA:
// put VS-IDs as keys
$temp = [];
foreach($dbCurrent as $key => $row){
	$temp[$row[3]] = $row;
}
$dbCurrent = $temp;

// CURRENT DATA:
// find all MVS
$mvsCurrent = [];
foreach($dbCurrent as $vsId => $row){
	$mvsName = $row[1];
	if($row[2] === ''){
		$mvsCurrent[$mvsName] = $vsId;
	}
}

// NEW DATA:
// find all MVS
$mvsSheet = [];
foreach($sheet as $key => $row){
	$mvsSheet[$row[3]] = substr($row[2], 3);
}

// CURRENT DATA:
// delete obsolete MVS entries
foreach($dbCurrent as $vsId => $row){
	if($row[2] === ''){
		if(!isset($mvsSheet[$row[1]])){
			unset($dbCurrent[$vsId]);
		}
	}
}

// CURRENT DATA:
// create new MVS entries
foreach($mvsSheet as $mvsName => $mvsId){
	if(!isset($mvsCurrent[$mvsName])){
		$dbCurrent[$mvsId] = [
			getIdCount($dbCurrent, 0),
			$mvsName,
			'',
			$mvsId,
			0,
			0
		];
	}
}

// CURRENT DATA:
// find all VS
$vsCurrent = [];
foreach($dbCurrent as $vsId => $row){
	if($row[2] !== ''){
		$vsCurrent[$vsId] = $row;
	}
}

// NEW DATA:
// find all VS
$vsSheet = [];
foreach($sheet as $key => $row){
	$vsSheet[substr($row[1], 2)] = $row;
}

// CURRENT DATA:
// delete obsolete VS entries
foreach($dbCurrent as $vsId => $row){
	if($row[2] !== ''){
		if(!isset($vsSheet[$vsId])){
			unset($dbCurrent[$vsId]);
		}
	}
}

// CURRENT DATA:
// create or overwrite existing VS entries, active flags have to be maintained
foreach($vsSheet as $vsId => $row){
	$dbCurrent[$vsId] = [
		getIdCount($dbCurrent, 0),
		$row[3],
		substr($row[2], 3),
		$vsId,
		isset($dbCurrent[$vsId]) ? $dbCurrent[$vsId][4] : 0,
		isset($dbCurrent[$vsId]) ? $dbCurrent[$vsId][5] : 0
	];
}

var_dump($dbCurrent);

// save file
$csvParser->createCsvFile($dbCurrent, 'csv_output/processed_data.csv');

// returns current id counter of the table + 1
function getIdCount($table, $keyOfId){
	$idCount = 0;
	foreach($table as $key => $row){
		$id = $row[$keyOfId];
		if($id > $idCount){
			$idCount = $id;
		}
	}
	return ++$idCount;
}
