<?php

class ParseCSV{
	private $content = NULL;
	private $delimiter = ',';
	private $enclosure = '"';
	private $escape = '\\';

	function __construct(array $params = array()){
		if(isset($params['delimiter'])){
			$this->delimiter = $params['delimiter'];
		}
		if(isset($params['enclosure'])){
			$this->enclosure = $params['enclosure'];
		}
		if(isset($params['escape'])){
			$this->escape = $params['escape'];
		}
	}

	public function getContent(){
		return $this->content;
	}

	public function setDelimiter($value){
		$this->delimiter = $value;
	}

	public function setEnclosure($value){
		$this->enclosure = $value;
	}

	public function setEscape($value){
		$this->escape = $value;
	}

	public function read($file){
		$csvData = file_get_contents(__DIR__ . '/' . $file);
		$rows = explode(PHP_EOL, $csvData);
		$result = array();

		foreach($rows as $row){
		    $result[] = str_getcsv($row, $this->delimiter, $this->enclosure, $this->escape);
		}
		$this->content = $result;
		return $this->content;
	}

	public function filterByRegex($pattern, $startIndex = 0, $endIndex = NULL){
		if($endIndex === NULL){
			$endIndex = count($this->content) - 1;
		}

		$result = array();

		for($index = $startIndex; $index <= $endIndex; $index++){
			$row = $this->content[$index];
			foreach($row as $entry){
				if(preg_match($pattern, $entry)){
					$result[] = $this->content[$index];
				}

			}
		}
		$this->content = $result;
		return $this->content;
	}

	public function createCsvFile($data, $path){
		$file = fopen($path, 'w');
		foreach($data as $row){
			fputcsv($file, $row);
  		}
		fclose($file);
	}
}
