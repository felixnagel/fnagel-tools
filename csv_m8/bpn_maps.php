<?php

include('ParseCSV.php');


$csvParser = new ParseCSV([
	'delimiter' => ',',
	'enclosure' => '"',
	'escape'    => '\\',
]);

$csvParser->read('csv_files/bpn_maps.csv');
$csvData = $csvParser->getContent();

//Debug::dump($csvData);

$resultsAlt = [];
$resultsNeu = [];
$resultsNicht = [];
$resultsVerschwunden = [];

// finde alle veränderten VS
foreach($csvData as $datum){
	if(isset($datum[3])){
		if($datum[3] == 'geändert'){
			$resultsAlt[$datum[1]] = $datum;
			$resultsNeu[$datum[2]] = $datum;
		}
	}
}

$resultsGeaendert = array_merge($resultsNeu, $resultsAlt);


// sammle alle nicht geänderten VS
foreach($csvData as $datum){
	if(isset($datum[3])){
		// finde alle veränderten VS
		if($datum[3] == 'nicht geändert'){
			$resultsNicht[$datum[1]] = $datum;
		}
	}
}

// finde alle verschwundenen VS
foreach($resultsGeaendert as $vs => $datum){
	if(!isset($resultsNicht[$vs]) && !isset($resultsNeu[$vs])){
		$resultsVerschwunden[$vs] = $datum;
	}
}

// var_dump($resultsGeaendert);
// var_dump($resultsAlt);
// var_dump($resultsNeu);
// var_dump($resultsNicht);
// var_dump($resultsVerschwunden);

array_unshift($resultsGeaendert, ['PLZ', 'VS alt', 'VS neu', 'status', 'MVS']);
array_unshift($resultsGeaendert, ['', '', '', '', '']);
$csvParser->createCsvFile($resultsGeaendert, 'csv_output/bpn_maps_geaendert.csv');
var_dump($resultsGeaendert);

array_unshift($resultsVerschwunden, ['PLZ', 'VS alt', 'VS neu', 'status', 'MVS']);
array_unshift($resultsVerschwunden, ['', '', '', '', '']);
$csvParser->createCsvFile($resultsVerschwunden, 'csv_output/bpn_maps_verschwunden.csv');
var_dump($resultsVerschwunden);

// finde alle für eine bestimmte MVS
$mvs01 = [];
foreach($csvData as $datum){
	if(isset($datum[3])){
		if($datum[4] == 'MVS01'){
			$mvs01[$datum[2]] = true;
		}
	}
}
var_dump($mvs01);



